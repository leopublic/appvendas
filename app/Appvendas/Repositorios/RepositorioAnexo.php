<?php
namespace Appvendas\Repositorios;

class RepositorioAnexo extends Repositorio
{

    /**
     * Redimensiona as imagens para o tamanho padrao
     * @return [type] [description]
     */
    public function ajustarTodasImagens() {
        set_time_limit(0);
        \Cache::forever('processando', 1);
        $produtos = \Produto::all();
        $tamCapa = \Configuracao::tamanho_capa();
        $tamThumb = \Configuracao::tamanho_thumb();
        $qualidade = \Configuracao::qualidade_jpeg();
        $qualidade = 100;

        $qtdTotal = \Produto::all()->count();
        $qtdTotal = $qtdTotal * 2;
        $processados = 0;
        $tam_antes = 0;
        $tam_depois = 0;
        $alterados = 0;

        \Cache::forever('tam_antes', 0);
        \Cache::forever('tam_depois', 0);
        \Cache::forever('total', $qtdTotal);
        \Cache::forever('processados', 0);
        \Cache::forever('alterados', 0);
        try {
            foreach ($produtos as $produto) {
            	try{
	                // Converte capa
	                $arq = $produto->caminhoCapa();
	                if (file_exists($arq)) {
				        $tam_a = filesize($arq);
				        if ($tam_a > 0){
				        	$tam_antes += $tam_a;
	                    	$this->ajustarImagem($arq, $tamCapa, $qualidade);
					        $tam_d = filesize($arq);
					        $tam_depois += $tam_d;
					        if ($tam_a != $tam_d){
					        	$alterados++;
					        }
				        }
	                }

	                // Converte thumb
	                $arq = $produto->caminhoThumb();
	                if (file_exists($arq)) {
				        $tam_a = filesize($arq);
				        if ($tam_a > 0){
				        	$tam_antes += $tam_a;
	                    	$this->ajustarImagem($arq, $tamThumb, $qualidade);
					        $tam_d = filesize($arq);
					        $tam_depois += $tam_d;
					        if ($tam_a != $tam_d){
					        	$alterados++;
					        }
				        }
	                }
            	}
            	catch (Exception $e){

            	}
	            $processados+=2;
		        \Cache::forever('processados', $processados);
		        \Cache::forever('alterados', $alterados);
	            \Cache::forever('tam_depois', $tam_depois);
		        \Cache::forever('tam_antes', $tam_antes);
            }
            \Cache::forever('processando', 0);
        }
        catch(Exception $e) {
            \Cache::forever('processando', 0);
        }
    }

    public function ajustarImagem($caminho, $tamanho, $qualidade) {

        // Load image and get image size.
	    $img = imagecreatefromjpeg($caminho);
        $width = imagesx($img);
        $height = imagesy($img);
        if ($width > $tamanho || $height > $tamanho) {
            if ($width > $height) {
                $newwidth = $tamanho;
                $divisor = $width / $tamanho;
                $newheight = floor($height / $divisor);
            }
            else {
                $newheight = $tamanho;
                $divisor = $height / $tamanho;
                $newwidth = floor($width / $divisor);
            }

            // Create a new temporary image.
            $tmpimg = imagecreatetruecolor($newwidth, $newheight);

            // Copy and resize old image into new image.
            imagecopyresampled($tmpimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            // Save thumbnail into a file.
            imagejpeg($tmpimg, $caminho, $qualidade);

            // release the memory
            imagedestroy($tmpimg);
        }
        imagedestroy($img);
    }

    public function progresso() {
        $total = \Cache::get('total', 0);
        $processados = \Cache::get('processados', 0);
        \log::info('lendo:'.$total.'/'.$processados);
        if ($total > 0) {
            $prog = ceil($processados / $total * 100);
        }
        else {
            $prog = 0;
        }
        $tam_antes = \Cache::get('tam_antes');
        $tam_depois = \Cache::get('tam_depois');
        if ($tam_antes > 0) {
            $red = ceil(($tam_depois / $tam_antes) * 100);
        }
        else {
            $red = 0;
        }
        $ret = array(
        		'progresso' 	=> $prog,
        		'processando' 	=> \Cache::get('processando', 0),
        		'processados' 	=> $processados,
        		'total' 	=> $total,
        		'tam_antes' 	=> $tam_antes,
        		'tam_depois' 	=> $tam_depois,
        		'alterados'		=> \Cache::get('alterados', 0),
        		'red' 			=> $red
        	);
        return $ret;
    }
}