<?php
namespace Appvendas\Repositorios;

class RepositorioFamilia extends Repositorio{
    
    public function exclua(\Familia $familia){
        \ProdutoFamilia::where('id_familia', '=', $familia->id_familia)->delete();
        $familia->delete();
    }

    public function edita($post){
        $id_familia = $post['id_familia'];
        if ($id_familia > 0){
            $familia = \Familia::find($id_familia);
        } else {
            $familia = new \Familia;            
        }
        $this->validador = new \Appvendas\Validadores\Familia($post);
        $this->validador->id_unique = $id_familia;
        if ($this->validador->passes()) {
            $familia->fill($post);
            $familia->save();
            
            return $familia;
        } else {
            return false;
        }
    }
}
