<?php
namespace Appvendas\Repositorios;

class RepositorioPerfil extends Repositorio{
    
    public function exclua(\Perfil $perfil){
        $perfil->delete();
    }

    public function edita($post){
        $id_perfil = $post['id_perfil'];
        if ($id_perfil > 0){
            $perfil = \Perfil::find($id_perfil);
        } else {
            $perfil = new \Perfil;
        }
        $this->validador = new \Appvendas\Validadores\Perfil($post);
        $this->validador->id_unique = $id_perfil;
        if ($this->validador->passes()) {
            $perfil->fill($post);
            $perfil->save();
            return true;
        } else {
            return false;
        }
    }
}
