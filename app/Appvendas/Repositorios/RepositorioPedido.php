<?php
namespace Appvendas\Repositorios;

class RepositorioPedido extends Repositorio{

    public function pedidosFiltro($ClienteID = '', $id_usuario = '', $dt_pedido_ini='', $dt_pedido_fim = ''){
        return \Pedido::doCliente($ClienteID)
                      ->doVendedor($id_usuario)
                      ->pedidoDesde($dt_pedido_ini)
                      ->pedidoAte($dt_pedido_fim)
                      ->orderBy('dt_pedido', 'desc')
                      ->paginate(20);
    }
    
    
    public function xmlTodosPedidosPendentes(){
        \Pedido::where('status_envio', '<>', 1)->update(array('status_envio' => null, 'msg_envio' => null));
        $pendentes = \Pedido::pendentes()->get();
        $ret = '';
        foreach($pendentes as $pedido){
            $ret.= $this->xmlPedido($pedido);
        }
        $ret = '<ListaPedidos>'.$ret.'<ListaPedidos>';
        return $ret;
    }
    
    /**
     * Formata o pedido em XML para o SIGNUS
     * @param \Pedido $pedido
     */
    public function xmlPedido(\Pedido $pedido){
        $ret = '<Pedido>';
        
        $ret .= $this->emtag('PrePedidoID', $pedido->id_pedido);
        $ret .= $this->emtag('ClienteID', $pedido->ClienteID);
        $ret .= $this->emtag('VendedorID', $pedido->vendedor->signus_id);
        $ret .= $this->emtag('UnidadeNegocioID', $pedido->UnidadeNegocioID);
        $ret .= $this->emtag('CondicaoPagamentoID', $pedido->CondicaoPagamentoID);
        $ret .= $this->emtag('DataVenda', $pedido->dt_pedido);
        $ret .= $this->emtag('NroPedidoCliente', $pedido->numero);
        
        $ret.= "<Itens>";
        $itens = $pedido->itens;
        foreach($itens as $itemPedido){
            $ret.= "<Item>";
            $ret .= $this->emtag('ProdutoID', $itemPedido->id_produto);
            $ret .= $this->emtag('Quantidade', $itemPedido->qtd);
            $ret .= $this->emtag('PrecoBruto', $itemPedido->preco_bruto);
            $ret .= $this->emtag('PrecoLiquido', $itemPedido->preco_liquido);
            $ret.= "</Item>";
        }
        $ret.= '</Itens>';
        
        $ret.= '</Pedido>';
        return $ret;
    }
    
    public function emtag($tag, $valor){
        return '<'.$tag.'>'.$valor.'</'.$tag.'>';
    }    
}