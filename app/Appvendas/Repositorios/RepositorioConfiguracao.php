<?php
namespace Appvendas\Repositorios;

class RepositorioConfiguracao extends Repositorio{
    //Retorna a versão mais recente do app disponível
    public function versaoMaisRecente(){
        $versaoAtual = \Versao::where('fl_atual', '=', 1)->first();
        return $versaoAtual->numero;
    }
}