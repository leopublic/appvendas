<?php

namespace Appvendas\Repositorios;

class Repositorio{
    protected $validador;
    protected $errors;

    public function registreErro($msg){
        $this->errors[] = $msg;
    }

    public function getErrors() {
        if (isset($this->errors)) {
            return $this->errors;
        } else {
            if (isset($this->validador)) {
                if (is_object($this->validador->getErrors())) {
                    return $this->validador->getErrors()->all();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

}