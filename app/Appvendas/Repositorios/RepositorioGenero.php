<?php
namespace Appvendas\Repositorios;

class RepositorioGenero extends Repositorio{
    
    public function exclua(\Genero $genero){
        \ProdutoGenero::where('id_genero', '=', $genero->id_genero)->delete();
        $genero->delete();
    }

    public function edita($post){
        $id_genero = $post['id_genero'];
        if ($id_genero > 0){
            $genero = \Genero::find($id_genero);
        } else {
            $genero = new \Genero;            
        }
        $this->validador = new \Appvendas\Validadores\Genero($post);
        $this->validador->id_unique = $id_genero;
        if ($this->validador->passes()) {
            $genero->fill($post);
            $genero->save();
            
            return $genero;
        } else {
            return false;
        }
    }
}
