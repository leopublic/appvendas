<?php

namespace Appvendas\Validadores;

abstract class Validador {

    protected $input;
    protected $errors;
    protected $id_unique;

    public function __construct($input = NULL) {
        $this->input = $input ? : \Input::all();
    }

    public function passes() {
        $validation = \Validator::make($this->input, $this->getRules(), $this->getMessages());
        if ($validation->passes()) {
            return true;
        }
        $this->errors = $validation->messages();
        return false;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getRules(){
        return static::$rules;
    }

    public function getMessages(){
        return static::$messages;
    }
}
