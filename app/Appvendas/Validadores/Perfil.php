<?php
namespace Appvendas\Validadores;
class Perfil extends Validador {
    public $id_unique;
    public static $rules = array(
        'descricao'       => 'required',
    );
    public static $messages = array(
        'descricao.required'       => 'O nome do perfil é obrigatório',
        'descricao.unique'       => 'Já existe outro perfil com o nome informado',
    );

    public function getRules(){
        if ($this->id_unique > 0){
            $x = array_merge(static::$rules , array('descricao' => 'required|unique:perfil,descricao,'.$this->id_unique.',id_perfil'));
        } else {
            $x = array_merge(static::$rules , array('descricao' => 'required|unique:perfil,descricao'));            
        }
        return $x;
    }
}
