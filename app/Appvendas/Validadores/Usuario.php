<?php
namespace Appvendas\Validadores;
class Usuario extends Validador {
    public $id_unique;
    public static $rules = array(
        'nome'       => 'required',
    );
    public static $messages = array(
        'nome.required'     => 'O nome do usuário é obrigatório',
        'email.required'    => 'O e-mail é obrigatório',
        'email.unique'      => 'Já existe outro usuário com o login escolhido',
    );

    public function getRules(){
        if ($this->id_unique > 0){
            $x = array_merge(static::$rules , array('email' => 'required|unique:usuario,email,'.$this->id_unique.',id_usuario'));
        } else {
            $x = array_merge(static::$rules , array('email' => 'required|unique:usuario,email'));            
        }
        return $x;
    }
}
