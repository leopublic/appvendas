<?php
namespace Appvendas\Validadores;
class Genero extends Validador {
    public $id_unique;
    public static $rules = array(
    );
    public static $messages = array(
        'nome.required'     => 'O nome do gênero é obrigatório',
        'nome.unique'     => 'Já existe um gênero com esse nome',
    );

    public function getRules(){
        if ($this->id_unique > 0){
            $x = array_merge(static::$rules , array('nome' => 'required|unique:genero,nome,'.$this->id_unique.',id_genero'));
        } else {
            $x = array_merge(static::$rules , array('nome' => 'required|unique:genero,nome'));            
        }
        return $x;
    }
}
