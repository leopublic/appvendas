<?php
namespace Appvendas\Validadores;
class Senha extends Validador {
    public static $rules = array(
        'password'                  => 'required|min:8|confirmed',
    );
    public static $messages = array(
        'password.required'         => 'Informe a nova senha',
        'password.min'         => 'A nova senha deve ter pelo menos 8 caracteres',
        'password.confirmed'        => 'As duas senhas não batem',
    );
}
