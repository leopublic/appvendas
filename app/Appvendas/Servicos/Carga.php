<?php
namespace Appvendas\Servicos;

class Carga extends Servico
{
    const iPROD_COD = 0;
    const iCATALOGO = 1;
    const iTITULO = 2;
    const iARTISTA = 3;
    const iPRECO = 4;
    const iCAMINHO_IMAGEM = 5;
    const iCAMINHO_THUMB = 6;
    const iTEXTO_CHEIO = 7;
    const iTEXTO_REDUZIDO = 8;
    const iICMS_ISENTO = 9;
    const iCOLETANEA = 10;
    const iLANCAMENTO = 11;
    const iMAIS_VENDIDO = 12;
    const iINFANTIL = 13;

    public static function caminhoSemCapa() {
        return public_path() . '/media/sem_capa.jpg';
    }

    public static function caminhoSemThumb() {
        return public_path() . '/media/sem_thumb.jpg';
    }

    public function carregar($raiz_destaque, $raiz_thumb, $colunas) {

        // Verifica se existe o produto
        $prod = \Produto::where('catalogo', '=', trim($colunas[self::iCATALOGO]))->first();

        // Senão existe cria
        if (!is_object($prod)) {
            $prod = new \Produto;
            $prod->catalogo = trim($colunas[self::iCATALOGO]);
            $prod->prod_cod = trim($colunas[self::iPROD_COD]);
            $prod->titulo = trim($colunas[self::iTITULO]);
            $prod->artista = trim($colunas[self::iARTISTA]);
            $prod->texto_cheio = trim($colunas[self::iTEXTO_CHEIO]);
            $prod->texto_reduzido = trim($colunas[self::iTEXTO_REDUZIDO]);
            $prod->coletanea = trim($colunas[self::iCOLETANEA]);
            $prod->mais_vendido = trim($colunas[self::iMAIS_VENDIDO]);
            $prod->lancamento = trim($colunas[self::iLANCAMENTO]);
            $prod->infantil = trim($colunas[self::iINFANTIL]);
            $prod->dt_importacao = date('Y-m-d H:i:s');
            $prod->id_status_produto = 2;
        }
        // Cria ou atualiza o produto
        $prod->preco = str_replace(",", ".", str_replace(".", "", trim($colunas[self::iPRECO])));
        $prod->icms_isento = trim($colunas[self::iICMS_ISENTO]);
        $prod->save();

        // Copia e renomeia os arquivos
        $prod->diretoriosOk();

        // Copia capa
        $colunas[self::iCAMINHO_IMAGEM] = str_replace('capas_th\\', '', $colunas[self::iCAMINHO_IMAGEM]);
        $origem = $raiz_destaque . trim($colunas[self::iCAMINHO_IMAGEM]);
        $destino = $prod->caminhoCapa();
        if (trim($colunas[self::iCAMINHO_IMAGEM]) != '') {
            if (file_exists($raiz_destaque . trim($colunas[self::iCAMINHO_IMAGEM]))) {
                if (file_exists($destino)) {
                    unlink($destino);
                }
                \File::copy($origem, $destino);
            }
            else {
                print "\n -->Arquivo " . $raiz_destaque . trim($colunas[self::iCAMINHO_IMAGEM]) . " não encontrado\n";
                $origem = self::caminhoSemCapa();
                \File::copy($origem, $destino);
            }
        }
        else {
            $origem = self::caminhoSemCapa();
            \File::copy($origem, $destino);
        }

        // Copia thumb
        $origem = $raiz_thumb . trim($colunas[self::iCAMINHO_THUMB]);
        $destino = $prod->caminhoThumb();
        if (trim($colunas[self::iCAMINHO_THUMB]) != '') {
            if (file_exists($raiz_thumb . trim($colunas[self::iCAMINHO_THUMB]))) {
                if (file_exists($destino)) {
                    unlink($destino);
                }
                \File::copy($origem, $destino);
            }
            else {
                print "\n -->Arquivo " . $raiz_thumb . trim($colunas[self::iCAMINHO_THUMB]) . " não encontrado";
                $origem = self::caminhoSemThumb();
                \File::copy($origem, $destino);
            }
        }
        else {
            $origem = self::caminhoSemThumb();
            \File::copy($origem, $destino);
        }
    }
}
