<?php
namespace Appvendas\Servicos;

class Seguranca extends Servico{

    public function rand_string($length = 10) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }
}