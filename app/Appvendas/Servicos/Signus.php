<?php
namespace Appvendas\Servicos;

class Signus{
    public $client;
    
    public function abreConexao(){
        //$url = 'http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc?wsdl';
        $url = \Configuracao::url_signus()."?wsdl";
        $this->client = new \SoapClient($url);    
    }
    
    public function clientesDoVendedor($id_usuario) {
        $usuario = \Usuario::find($id_usuario);
        $unidade_negocio_id = \Configuracao::UnidadeNegocioID();
        $params = array('strXML' => '<?xml version="1.0"?><Solicitacao><VendedorID>'.$usuario->signus_id.'</VendedorID><UnidadeNegocioID>'.$unidade_negocio_id.'</UnidadeNegocioID></Solicitacao>');
        $clientesXml = $this->client->__soapCall('ObterClientes', array($params))->ObterClientesResult;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $clientesXml));
        return $retorno->ListaClientes->Cliente;
    }
    
    public function condicoesDoVendedor($id_usuario) {
        $usuario = \Usuario::find($id_usuario);
        $unidade_negocio_id = \Configuracao::UnidadeNegocioID();
        $params = array('strXML' => '<?xml version="1.0"?><Solicitacao><VendedorID>'.$usuario->signus_id.'</VendedorID><UnidadeNegocioID>'.$unidade_negocio_id.'</UnidadeNegocioID></Solicitacao>');
        $clientesXml = $this->client->__soapCall('ObterCondicoesPagamento', array($params))->ObterCondicoesPagamentoResult;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $clientesXml));
        return $retorno->ListaCondicoesPagamento->CondicaoPagamento;
    }
    
    public function carregaClientes($VendedorID, $UnidadeNegocioID) {
        $params = array('strXML' => '<?xml version="1.0"?><Solicitacao><VendedorID>'.$VendedorID.'</VendedorID><UnidadeNegocioID>'.$UnidadeNegocioID.'</UnidadeNegocioID></Solicitacao>');
        $clientesXml = $this->client->__soapCall('ObterClientes', array($params))->ObterClientesResult;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $clientesXml));
        $clientes = $retorno->ListaClientes->Cliente;
        foreach($clientes as $clienteXml){
            $cliente = \Cliente::find($clienteXml->ClienteID);
            if (!is_object($cliente)){
                $cliente = new \Cliente;                
                $cliente->ClienteID = $clienteXml->ClienteID;
            } 
            $cliente->Codigo = $clienteXml->Codigo;
            $cliente->CNPJ = $clienteXml->CNPJ;
            $cliente->RazaoSocial = $clienteXml->RazaoSocial;
            $cliente->NomeFantasia = $clienteXml->NomeFantasia;
            $cliente->Endereco = $clienteXml->Endereco;
            $cliente->Bairro = $clienteXml->Bairro;
            $cliente->CEP = $clienteXml->CEP;
            $cliente->MunicipioID = $clienteXml->MunicipioID;
            $cliente->Municipio = $clienteXml->Municipio;
            $cliente->EstadoID = $clienteXml->EstadoID;
            $cliente->EstadoSigla = $clienteXml->EstadoSigla;
            $cliente->save();
        }
    }
    
    public function carregaCondicaoPagamento($VendedorID, $UnidadeNegocioID) {
        $params = array('strXML' => '<?xml version="1.0"?><Solicitacao><VendedorID>'.$VendedorID.'</VendedorID><UnidadeNegocioID>'.$UnidadeNegocioID.'</UnidadeNegocioID></Solicitacao>');
        $xml = $this->client->__soapCall('ObterCondicoesPagamento', array($params))->ObterCondicoesPagamentoResult;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $xml));
        $conds = $retorno->ListaCondicoesPagamento->CondicaoPagamento;
        foreach($conds as $condXml){
            $cond = \CondicaoPagamento::find($condXml->CondicaoPagamentoID);
            if (!is_object($cond)){
                $cond = new \CondicaoPagamento();                
                $cond->CondicaoPagamentoID = $condXml->CondicaoPagamentoID;
            } 
            $cond->Descricao = $condXml->Descricao;
            $cond->save();
        }
    }

    public function enviaPedidosPendentes(){
        $rep = new \Appvendas\Repositorios\RepositorioPedido;
        $xml = $rep->xmlTodosPedidosPendentes();
        
        $params = array('strXML' => '<?xml version="1.0"?>'.$xml);
        $result = $this->client->__soapCall('RegistrarPedidos', array($params))->RegistrarPedidosResult;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $result));
        $pedidos = $retorno->ListaPedidos->Pedido;
        foreach($pedidos as $pedidoXml){
            $pedido = \Pedido::find($pedidoXml->PrePedidoID);
            $pedido->msg_envio = $pedidoXml->msg_envio;
            $pedido->status_envio = $pedidoXml->status_envio;
            $pedido->save();
        }
    }

    public function enviaUmPedido($pedido){
        $rep = new \Appvendas\Repositorios\RepositorioPedido;

        $xml = '<ListaPedidos>'.$rep->xmlPedido($pedido).'</ListaPedidos>';
        
        print htmlentities($xml);
        $params = array('strXML' => '<?xml version="1.0"?>'.$xml);
        $result = $this->client->__soapCall('RegistrarPedidos', array($params))->RegistrarPedidosResult;
        print htmlentities($result);
        exit;
        $retorno = simplexml_load_string(iconv('UTF-8', 'UTF-8', $result));
        var_dump($retorno);
        $pedidos = $retorno->ListaPedidos->Pedido;
        foreach($pedidos as $pedidoXml){
            $pedido = \Pedido::find($pedidoXml->PrePedidoID);
            $pedido->msg_envio = $pedidoXml->msg_envio;
            $pedido->status_envio = $pedidoXml->status_envio;
            $pedido->save();
        }
        
    }
    
}