@extends('base')

@section('content')
<div class="page-title">
    <div>
        <h1><i class="icon-link"></i>
            @if ($model->id_familia > 0)
            Alterar família
            @else
            Criar nova família
            @endif
        </h1>
    </div>
</div>
@include('html/mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-pencil"></i> 
                    @if ($model->id_familia > 0)
                    {{$model->nome}}
                    @else
                    (novo)
                    @endif
                </h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                {{ Form::hidden('id_familia', $model->id_familia, array('id' => 'id_familia')) }}
                @include('html/campo-texto', array('id' => 'nome', 'label' => 'Nome', 'valor' => $model->nome,  'help' => '',  array()))

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-music"></i> Produtos da família</h3>
                <div class="box-tool">
                    <button class="btn btn-primary" id="adicionar"  onclick="frmPesquisaShow();"> Adicionar</button>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 180px">Ação</th>
                            <th style="width: 80px; text-align: center;">Cat.</th>
                            <th style="width: auto;">Artista</th>
                            <th style="width: auto;">Título</th>
                            <th style="width: auto; text-align: center;">Lançamento?</th>
                        </tr>
                    </thead>
                    @if(isset($relacionados))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($relacionados as $reg)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::to('/produto/cadastro/'.$reg->id_produto) }}" title="Detalhes do produto"><i class="icon-pencil"></i></a>
                                <a class="btn btn-danger" href="{{ URL::to('/familia/remover/'.$model->id_familia.'/'.$reg->id_produto) }}" title="Remover produto da familia"><i class="icon-trash"></i></a>
                            </td>
                            <td style="text-align: center;">{{ $reg->catalogo }}</td>
                            <td>{{ $reg->artista }}</td>
                            <td>{{ $reg->titulo }}</td>
                            <td style="text-align: center;">@if ($reg->lancamento)
                                <i class="icon-check"></i>
                                @else
                                <i class="icon-check-empty"></i>                                
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>

<div id="frmModal" class="modal fade" tabindex="-1"  data-width="750" data-height="650">
</div>

@stop

@section('scripts')

<script type="text/javascript">

    // Exibe o painel de inclusão de titular

    function frmPesquisaShow() {
        $('#adicionar').html('<i class="icon-spin icon-spinner"></i> Aguarde');
        $('#adicionar').attr('disabled', true);
        var url = "/familia/formpesquisa/";
        $("#frmModal").load(url, '', function () {
            $('#frmModal').modal();
            $('#adicionar').html('Adicionar');
            $('#adicionar').attr('disabled', false);
        });
    }


    // Recupera a pesquisa de titulares
    function btnPesquisarClick() {
        $('#btnPesquisar').html('<i class="icon-spin icon-spinner"></i> Aguarde');
        $('#btnPesquisar').attr('disabled', true);
        var id_familia = $('#id_familia').val();
        var url = "/familia/pesquisar/" + id_familia + "/" + $('#frmModal #titulo').val();
        $("#frmModal").load(url, '', function () {
            $('#btnPesquisar').html('Pesquisar');
            $('#btnPesquisa').attr('disabled', false);
        });
    }

    function getAdicionarProduto(id_produto) {
        $('#btn' + id_produto).html('<i class="icon-spin icon-spinner"></i>');
        $('#btn' + id_produto).attr('disabled', true);
        var id_familia = $('#id_familia').val();
        var url = '/familia/adicionar/'+id_familia+'/'+id_produto;
        $.ajax({
            method: "GET",
            url: url,
            dataType: "json"
        })
        .done(function (data) {
            if (data.codret == 1){
                $('#btn' + id_produto).html('<i class="icon-ok"></i>');
                $('#btn' + id_produto).removeClass('btn-primary');
                $('#btn' + id_produto).addClass('btn-warning');
            } else {
                $('#btn' + id_produto).html('<i class="icon-exclamation"></i>');
                $('#btn' + id_produto).removeClass('btn-primary');
                $('#btn' + id_produto).addClass('btn-danger');
                alert("Nao foi possivel adicionar o produto "+data.msg);
            }    

        });
    }
</script>
@stop