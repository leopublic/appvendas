<div class="modal-dialog modal-lg">
    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Adicionar produto a familia</h4>
        </div>
        <div class="modal-body">
            {{Form::open(array('role' => "form", "class" => "form-horizontal", "id" => "fmrObraPesquisar"))}}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Título</label>
                            <div class="col-md-9">
                                {{Form::text('titulo', '', array("class" => "form-control", "id"=> 'titulo'))}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if($produtos)
                    <table class="table table-condensed">
                        @foreach($produtos as $produto)
                        <tr>
                            <td><a id="btn{{$produto->id_produto}}" href="#" onclick="getAdicionarProduto('{{$produto->id_produto}}');" class="btn btn-primary"><i class="icon-plus"></i></a></td>
                            <td>{{$produto->catalogo}}</td>
                            <td>{{$produto->titulo}}</td>
                            <td>{{$produto->artista}}</td>
                        </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
                <div class="row">
                    <div id="msg" style="color:red;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="javascript:btnPesquisarClick();" id="btnPesquisar">Pesquisar</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Cancelar</button>
                </div>            
            </div>
            {{Form::close()}}
        </div>
    </div>
</div>