@extends('base')

@section('content')
    @include('html/page-title', array('titulo' => 'Gestão de conteúdo do catálogo dos representantes', 'subTitulo' => ''))

    @include('html/mensagens')

    <div class="row">
        <div class="col-md-3">
            <a class="tile tile-light-blue" href="{{URL::to('/produto')}}">
                <div class="img">
                    <i class="icon-music"></i>
                </div>
                <div class="content">
                    <p class="title">Produtos</p>
                </div>
            </a>
        </div>

    </div>

@stop