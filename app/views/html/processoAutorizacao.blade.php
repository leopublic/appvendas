@include('html/campo-texto', array('qtdCol' => 2, 'id' => 'numeroProcesso', 'label' => 'Número processo', 'valor' => $processo->numeroProcesso, 'help' => '', array()))
@include('html/campo-data', array('qtdCol' => 2,'id' => 'dataRequerimento', 'label' => 'Data requerimento', 'valor' => $processo->dataRequerimento, 'help' => '', array()))
@include('html/campo-texto', array('qtdCol' => 2,'id' => 'numeroOficio', 'label' => 'Número ofício', 'valor' => $processo->numeroOficio,  'help' => '', array()))
@include('html/campo-texto', array('qtdCol' => 2,'id' => 'prazoSolicitado', 'label' => 'Prazo solicitado', 'valor' => $processo->prazoSolicitado,  'help' => '', array()))
