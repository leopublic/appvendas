<!-- BEGIN campo-texto -->
	<div class="form-group">
	  {{Form::label($id, $label, array('id' => $id, 'class' => 'col-sm-3 col-lg-2 control-label')) }}
	   <div class="col-sm-9 col-lg-10 controls">
	      {{ Form::checkbox($id, Input::old($id, $valor), $attributes = array('class' => 'form-control')); }}
	      @if(isset($help))
	      <span class="help-inline">{{ $help }}</span>
	      @endif
	   </div>
	</div>
<!-- END campo-texto -->
