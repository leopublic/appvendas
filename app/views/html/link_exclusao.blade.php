@if (!isset($estilo))
<? $estilo = "btn-danger btn"; ?>
@endif
@if (!isset($icon))
<? $icon = "icon-trash"; ?>
@endif
@if (!isset($title))
<? $title = ""; ?>
@endif


<div class="btn-group">
    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle {{$estilo}}"><i class="{{$icon}}" title="{{$title}}"></i></a>
    <ul class="dropdown-menu btn-danger">
        <li><a href="{{ URL::to($url) }}">{{ $msg_confirmacao }}</a></li>
    </ul>
</div>

