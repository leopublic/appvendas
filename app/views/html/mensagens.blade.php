<!-- BEGIN mensagens -->
    @if(isset($errors) )
      @if(is_object($errors))
        @if(count($errors->all())> 0)
        <div class="row">
            <div class="col-md-12">
              <div class="alert alert-danger">
                Não foi possível executar a operação: 
                <ul class="errors">
                  @foreach($errors->all() as $message)
                  <li>{{ $message }}</li>
                  @endforeach
                </ul>
              </div>
            </div>
        </div>
        @endif
      @else
        @if (is_array($errors))
            <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-danger">
                    Não foi possível executar a operação: 
                    <ul class="errors">
                      @foreach($errors as $erro)
                      <li>{{ $erro }}</li>
                      @endforeach
                    </ul>
                  </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-danger">
                    <ul class="errors">
                      <li>{{ $errors }}</li>
                    </ul>
                  </div>
                </div>
            </div>
        @endif
      @endif
    @endif
    @if (Session::has('flash_error'))
    <div class="row">
        <div class="col-md-12">
          <div class="alert alert-danger">
            {{ Session::get('flash_error') }}
          </div>
        </div>
    </div>
    @endif
    @if (Session::has('flash_msg'))
    <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success">
            {{ Session::get('flash_msg') }}
          </div>
        </div>
    </div>
    @endif
<!-- END mensagens -->
