<!-- BEGIN campo-texto -->
@if(!isset($qtdCol) || $qtdCol == '1')
	<div class="form-group">
	  {{Form::label($id, $label, array('id' => $id, 'class' => 'col-sm-3 col-lg-2 control-label')) }}
	   <div class="col-sm-9 col-lg-10 controls">
                <input type="password" name="{{$id}}" class="form-control"/>
	      @if(isset($help))
	      <span class="help-inline">{{ $help }}</span>
	      @endif
	   </div>
	</div>
@else
	@if($qtdCol == '2')
	<div class="col-md-6">
		<div class="form-group ">
		  {{Form::label($id, $label, array('id' => $id, 'class' => 'col-sm-5 col-lg-3 control-label')) }}
		   <div class="col-sm7 col-lg-9 controls">
                       <input type="password" name="{{$id}}" class="form-control"/>
		      @if(isset($help))
		      <span class="help-inline">{{ $help }}</span>
		      @endif
		   </div>
		</div>
	</div>
	@endif	
@endif	
<!-- END campo-texto -->
