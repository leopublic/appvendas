<!-- BEGIN campo-texto -->
@if(!isset($qtdCol) || $qtdCol == '1')
	<div class="form-group">
	  {{Form::label($id, $label, array('id' => $id, 'class' => 'col-sm-3 col-lg-2 control-label')) }}
	   <div class="col-sm-5 col-lg-3 controls">
			{{ Form::text($id, Input::old($id, $valor), $attributes = array('class' => 'form-control date-picker', 'data-date-format' => 'dd/mm/yyyy')); }}
	      @if(isset($help))
	      <span class="help-inline">{{ $help }}</span>
	      @endif
	   </div>
	</div>
@else
	@if($qtdCol == '2')
	<div class="col-md-6">
		<div class="form-group">
		  {{Form::label($id, $label, array('id' => $id, 'class' => 'col-sm-5 col-lg-3 control-label')) }}
		   <div class="col-sm-7 col-lg-9 controls">
				{{ Form::text($id, Input::old($id, $valor), $attributes = array('class' => 'form-control date-picker', 'data-date-format' => 'dd/mm/yyyy')); }}
		      @if(isset($help))
		      <span class="help-inline">{{ $help }}</span>
		      @endif
		   </div>
		</div>
	</div>
	@endif
@endif
<!-- END campo-texto -->
