@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Importar XML"</h1>
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Informe o arquivo</h3>
            </div>

            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal", "files"=> true)) }}
                {{Form::hidden('id_versao', 0)}}
                    <div class="form-group">
                        <div class="controls">
                            <label class="col-sm-3 col-lg-2 control-label">XML</label>
                            <div class="col-sm9 col-lg-10 controls">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-group">
                                        <div class="form-control uneditable-input">
                                            <i class="icon-file fileupload-exists"></i> 
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <div class="input-group-btn">
                                            <a class="btn bun-default btn-file">
                                                <span class="fileupload-new">Selecione o arquivo</span>
                                                <span class="fileupload-exists">Alterar</span>
                                                <input type="file" name="arquivoxml" class="file-input"/>
                                            </a>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Importar</button>
                            </div>
                        </div>
                    {{Form::close()}}
                    </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript" src="/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
    $('#menuTodasVersoes').addClass('active');
});
</script>

@stop