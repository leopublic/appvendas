@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Produtos bloqueados', 'subTitulo' => '', 'icon' => 'icon-lock'))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array("class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'titulo', 'label' => 'Título', 'valor' => Input::old('titulo'),  'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'artista', 'label' => 'Artista', 'valor' => Input::old('artista'),  'help' => '',  array()))
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Produtos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
            @if(isset($registros))
                {{$registros->links()}}
            @endif
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 240px">Ação</th>
                            <th style="width: 80px; text-align: center;">Cat.</th>
                            <th style="width: auto;">Artista</th>
                            <th style="width: auto;">Título</th>
                            <th style="width: auto; text-align: center;">Lançamento?</th>
                            <th style="width: auto;">Thumb</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $reg)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::to('/produto/cadastro/'.$reg->id_produto) }}" title="Detalhes do produto"><i class="icon-pencil"></i></a>
                                <a class="btn btn-lime" href="{{ URL::to('/produto/musicas/'.$reg->id_produto) }}" title="Músicas"><i class="icon-volume-up"></i></a>
                                <a class="btn btn-warning" href="{{ URL::to('/produto/videos/'.$reg->id_produto) }}" title="Vídeos"><i class="icon-film"></i></a>
                                <a class="btn btn-magenta" href="{{ URL::to('/produto/eventos/'.$reg->id_produto) }}" title="Eventos"><i class="icon-calendar"></i></a>
                                <a class="btn btn-primary" href="{{ URL::to('/produto/liberar/'.$reg->id_produto) }}" title="Liberar esse produto"><i class="icon-unlock"></i></a>
                            </td>
                            <td style="text-align: center;">{{ $reg->catalogo }}</td>
                            <td>{{ $reg->artista }}</td>
                            <td>{{ $reg->titulo }}</td>
                            <td style="text-align: center;">@if ($reg->lancamento)
                                <i class="icon-check"></i>
                                @else
                                <i class="icon-check-empty"></i>                                
                                @endif
                            </td>
                            <td style="text-align: center;"><img src="{{ $reg->urlThumb() }}" style="max-width:65px;" /></td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript">

$(document).ready(function(){
    $('#menuProdutosBloqueados').addClass('active');
});
</script>
@stop
