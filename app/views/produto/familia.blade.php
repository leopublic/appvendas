@extends('produto/show')

@section('aba')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-list"></i> Produtos associados</h3>
                </div>
                <div class="box-content">
                    @if(count($obj->relacionados()) > 0)
                    <table class="table table-striped table-hover fill-head">
                        <thead>
                            <tr>
                                <th style="width: 40px">#</th>
                                <th style="width: 100px">Família</th>
                                <th style="width: 100px">Código</th>
                                <th style="width: 100px">Catálogo</th>
                                <th style="width: auto">Título</th>
                                <th style="width: auto">Artista</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? $i = 0; ?>
                            @foreach($obj->relacionados() as $produto)
                            <? $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td><a href="/familia/edite/{{$produto->id_familia}}">{{$produto->nome}}</a>
                                    
                                </td>
                                <td>{{ $produto->prod_cod }}</td>
                                <td>{{ $produto->catalogo }}</td>
                                <td>{{ $produto->titulo }}</td>
                                <td>{{ $produto->artista }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    (nenhum evento encontrado)
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
