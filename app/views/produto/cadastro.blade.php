@extends('produto/show')

@section('aba')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-edit"></i> Descrição do produto</h3>
                </div>
                <div class="box-content">
                    {{ Form::open(array( "class"=>"form-horizontal")) }}
                    {{ Form::hidden('id_produto', $obj->id_produto, array('id' => 'id_produto')) }}
                    @include('html/campo-texto', array('id' => 'catalogo', 'label' => 'Catálogo', 'valor' => $obj->catalogo, 'help' => '', 'opcoes' => array('disabled')))
                    @include('html/campo-texto', array('id' => 'prod_cod', 'label' => 'Código', 'valor' => $obj->prod_cod, 'help' => '', 'opcoes' => array('disabled')))
                    @include('html/campo-texto', array('id' => 'titulo', 'label' => 'Título', 'valor' => $obj->titulo, 'help' => '', 'opcoes' => array('disabled')))
                    @include('html/campo-texto', array('id' => 'artista', 'label' => 'Artista', 'valor' => $obj->artista, 'help' => '', 'opcoes' => array('disabled')))
                    @include('html/campo-texto', array('id' => 'preco', 'label' => 'Preço', 'valor' => 'R$ '.number_format($obj->preco, 2, ",", "."), 'help' => '', 'opcoes' => array('disabled')))
                    @include('html/campo-texto', array('id' => 'codigo_barras', 'label' => 'Código de barras', 'valor' => $obj->codigo_barras, 'help' => ''))
                    @include('html/campo-select', array('id' => 'id_classe', 'label' => 'Classe', 'valores' => $classes, 'valor' => $obj->id_classe,  'help' => '',  array()))
                    @include('html/campo-textarea', array('id' => 'texto_cheio', 'label' => 'Descrição', 'valor' => $obj->texto_cheio, 'help' => ''))
                    @include('html/campo-textarea', array('id' => 'texto_reduzido', 'label' => 'Descrição (red.)', 'valor' => $obj->texto_reduzido, 'help' => ''))
                   <div class="form-group">
                      {{Form::label('lancamento','Lançamento?' , array('id' => 'lancamento', 'class' => 'col-sm-3 col-lg-2 control-label')) }}
                       <div class="col-sm-9 col-lg-10 controls">
                           {{Form::hidden('lancamento', 0)}}
                           <label class="radio-inline">
                              {{ Form::radio('lancamento', 1, (Input::old('lancamento', $obj->lancamento) ? true : false) ) }} Sim
                           </label>
                           <label class="radio-inline">
                              {{ Form::radio('lancamento', 0, (Input::old('lancamento', $obj->lancamento) ? false: true)) }} Não
                           </label>
                       </div>
                    </div>
                    <div class="form-group">
                      {{Form::label('icms_isento','Isento ICMS?' , array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                       <div class="col-sm-9 col-lg-10 controls">
                           {{Form::hidden('icms_isento', 0)}}
                           <label class="radio-inline">
                              {{ Form::radio('icms_isento',  1, (Input::old('icms_isento', $obj->icms_isento) ? true : false)) }} Sim
                           </label>
                           <label class="radio-inline">
                              {{ Form::radio('icms_isento',  0, (Input::old('icms_isento', $obj->icms_isento) ? false : true)) }} Não
                           </label>
                       </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('id_genero', 'Gêneros', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                        <div class="col-sm-9 col-lg-10 controls">
                            <div class="row">
                                @foreach(Genero::orderBy('nome')->get() as $genero)
                                <div class="col-md-3">
                                    <input type="checkbox" name="id_genero[]" value="{{$genero->id_genero}}"
                                           @if (in_array($genero->id_genero, $obj->arrayDeGeneros() ) )
                                           checked="checked"
                                           @endif
                                           /> {{$genero->nome}}
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>

@stop
