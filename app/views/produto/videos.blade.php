@extends('produto/show')

@section('aba')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-plus"></i> Adicionar vídeos</h3>
                </div>
                <div class="box-content">
                    <form action="/anexo/novovideo/{{$obj->id_produto}}" class=" dropzone hidden-xs dz-clickable" id="my-awesome-dropzone" method="POST">
                        <div class="dz-message" style="text-align:center;height:">
                            Arraste os arquivos para cá ou clique para selecionar
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-paper-clip"></i> Vídeos cadastrados</h3>
                    {{Form::hidden('id_produto', $obj->id_produto, array('id' => 'id_produto')) }}
                </div>
                <div class="box-content" id="containerAnexos">
                    @include('anexo.index', array('anexos' => $obj->anexos))
                </div>
            </div>
        </div>
    </div>

@stop
