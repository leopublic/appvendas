@extends('produto/show')

@section('aba')
{{ Form::open(array('url' => 'anexo/storecapas', "class"=>"form-horizontal", "files"=>true)) }}
{{ Form::hidden('id_produto', $obj->id_produto, array('id' => 'id_produto')) }}
<div class="form-group">
    <label class="col-sm-3 col-lg-2 control-label">Imagem em destaque</label>
    <div class="col-sm-9 col-lg-10 controls">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                <img src="{{$obj->urlCapa()}}" alt="" />
            </div>
            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-default btn-file"><span class="fileupload-new">Selecione um arquivo</span>
                    <span class="fileupload-exists">Alterar</span>
                    <input type="file" name="capa" class="file-input" /></span>
                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remover</a>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 col-lg-2 control-label">Imagem thumbnail</label>
    <div class="col-sm-9 col-lg-10 controls">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">

                <img src="{{$obj->urlThumb()}}" alt="" />
            </div>
            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                <span class="btn btn-default btn-file"><span class="fileupload-new">Selecione um arquivo</span>
                    <span class="fileupload-exists">Alterar</span>
                    <input type="file" name="capa_thumb" class="file-input" /></span>
                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remover</a>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
    </div>
</div>
{{ Form::close()}}

@stop
