@extends('produto/show')

@section('aba')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-plus"></i> Adicionar música</h3>
            </div>
            <div class="box-content">
                {{ Form::open(array("url"=>"/musica/store", "class"=>"form-horizontal", "files"=> true)) }}
                {{Form::hidden('id_produto', $obj->id_produto)}}
                    @include('html/campo-texto', array('id' => 'ordem', 'label' => 'Ordem', 'valor' => Input::old('ordem'), 'help' => '', 'opcoes' => array()))
                    @include('html/campo-texto', array('id' => 'nome', 'label' => 'Nome', 'valor' => Input::old('nome'), 'help' => '', 'opcoes' => array()))
                    @include('html/campo-texto', array('id' => 'duracao', 'label' => 'Duração', 'valor' => Input::old('duracao'), 'help' => '', 'opcoes' => array()))
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Trecho</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-group">
                                    <div class="form-control uneditable-input">
                                        <i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <div class="input-group-btn">
                                        <a class="btn bun-default btn-file">
                                            <span class="fileupload-new">Selecione o arquivo</span>
                                            <span class="fileupload-exists">Alterar</span>
                                            <input type="file" name="trecho" class="file-input"/>
                                        </a>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Cancelar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        </div>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@if(is_object($obj->musicas ))
<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th style="width: 80px; text-align: center;">Ações</th>
            <th style="width: 40px">#</th>
            <th style="width: auto">Nome</th>
            <th style="width: auto;">Duração</th>
            <th style="width: auto;">Trecho</th>
            <th style="width: auto;">Adicionar/substituir trecho</th>
        </tr>
    </thead>
    <tbody>
        <? $i = 0; ?>
        @foreach($obj->musicas as $musica)
        <? $i++; ?>
        <tr>
            <td style="text-align: center;">
                @include('html.link_exclusao', array('url'=> '/musica/exclua/'.$musica->id_musica, 'msg_confirmacao' => 'Clique para confirmar a exclusão dessa música', 'title' => 'Excluir', 'icon'=>'icon-trash'))
            </td>
            <td>{{ $musica->ordem }}</td>
            <td>{{ $musica->nome }}</td>
            <td>{{ $musica->duracao }}</td>
            <td>@if ($musica->tem_trecho())
               	<audio controls="controls" src="{{ $musica->url()}}">
                (player não suportado)
                </audio>
                @else
                (não disponível)
                @endif
            </td>
            <td>
                {{ Form::open(array("url"=>"/musica/trecho", "class"=>"form-", "files"=> true)) }}
                {{Form::hidden('id_musica', $musica->id_musica)}}
                    <div class="form-group">
                        <div class="controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-group">
                                    <div class="form-control uneditable-input">
                                        <i class="icon-file fileupload-exists"></i> 
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <div class="input-group-btn">
                                        <a class="btn bun-default btn-file">
                                            <span class="fileupload-new">Selecione o arquivo</span>
                                            <span class="fileupload-exists">Alterar</span>
                                            <input type="file" name="trecho" class="file-input"/>
                                        </a>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Cancelar</a>
                                    </div>
                                    <button type="submit" class="btn btn-primary"><i class="icon-cloud-upload"></i> Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {{Form::close()}}                
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
(nenhuma música encontrada)
@endif

@stop
