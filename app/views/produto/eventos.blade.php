@extends('produto/show')

@section('aba')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-plus"></i> Adicionar evento</h3>
                </div>
                <div class="box-content">
                    {{ Form::open(array('url' => 'evento/store', "class"=>"form-horizontal")) }}
                    {{ Form::hidden('id_produto', $obj->id_produto, array('id' => 'id_produto')) }}
                    {{ Form::hidden('id_evento', $evento->id_evento, array('id' => 'id_evento')) }}
                    @include('html/campo-data', array('id' => 'dt_agendada_fmt', 'label' => 'Data', 'valor' => $evento->dt_agendada_fmt, 'help' => '', array()))
                    @include('html/campo-texto', array('id' => 'titulo', 'label' => 'Título', 'valor' => $evento->titulo, 'help' => '', array()))
                    @include('html/campo-textarea', array('id' => 'descricao', 'label' => 'Descrição', 'valor' => nl2br($evento->descricao),  'help' => '', array()))
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-list"></i> Eventos cadastrados</h3>
                </div>
                <div class="box-content">
                    @if(count($obj->eventos) > 0)
                    <table class="table table-striped table-hover fill-head">
                        <thead>
                            <tr>
                                <th style="width: 40px">#</th>
                                <th style="width: 100px">Ação</th>
                                <th style="width: auto">Data</th>
                                <th style="width: auto">Título</th>
                                <th style="width: auto">Descricao</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? $i = 0; ?>
                            @foreach($obj->eventos as $evento)
                            <? $i++; ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="{{ URL::to('/produto/eventos/'.$evento->id_produto.'/'.$evento->id_evento) }}"><i class="icon-edit"></i></a>
                                    @include('html.link_exclusao', array('url' => '/evento/del/'.$evento->id_evento,'icon'=>'icon-trash', 'msg_confirmacao' => 'Clique para confirmar a exclusão desse evento'))
                                </td>
                                <td>{{ $evento->dt_agendada_fmt }}</td>
                                <td>{{ $evento->titulo }}</td>
                                <td>{{ $evento->descricao }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    (nenhum evento encontrado)
                    @endif
                </div>
            </div>
        </div>
    </div>

@stop
