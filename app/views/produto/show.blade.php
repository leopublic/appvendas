@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>{{$obj->artista}}: "{{$obj->titulo}}" ({{$obj->catalogo}})</h1>
</div>
@if ($obj->novo)
<div class="row">
    <div class="col-md-12">
        <a href="/produto/liberar/{{$obj->id_produto}}" class="btn btn-primary pull-right"><i class="icon-unlock"></i> Liberar</a>
    </div>
</div>
@else 
<div class="row">
    <div class="col-md-12">
        <a href="/produto/bloquear/{{$obj->id_produto}}" class="btn btn-danger pull-right"><i class="icon-lock"></i> Bloquear</a>
    </div>
</div>
@endif
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                @if (!isset($icon))
                <? $icon = 'icon-folder-open'; ?>
                @endif
                
                <h3><i class="{{$icon}}"></i> {{$nome_aba}}</h3>
                <ul class="nav nav-tabs hidden-xs">
                    <li {{($aba_ativa == 'cadastro') ? print 'class="active"' : ''}}><a href="/produto/cadastro/{{ $obj->id_produto}}" >Cadastro</a></li>
                    <li {{($aba_ativa == 'capas') ? print 'class="active"' : ''}}><a href="/produto/capas/{{ $obj->id_produto}}" >Capas</a></li>
                    <li {{($aba_ativa == 'musicas') ? print 'class="active"' : ''}}><a href="/produto/musicas/{{ $obj->id_produto}}" >Músicas</a></li>
                    <li {{($aba_ativa == 'videos') ? print 'class="active"' : ''}}><a href="/produto/videos/{{ $obj->id_produto}}" >Vídeos</a></li>
                    <li {{($aba_ativa == 'eventos') ? print 'class="active"' : ''}}><a href="/produto/eventos/{{ $obj->id_produto}}" >Eventos</a></li>
                    <li {{($aba_ativa == 'familia') ? print 'class="active"' : ''}}><a href="/produto/familia/{{ $obj->id_produto}}" >Família</a></li>
                </ul>

            </div>

            <div class="box-content">
                @yield('aba')
            </div>
        </div>
    </div>
</div>

</div>




@stop

@section('scripts')
<script type="text/javascript" src="/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    $('#menuProdutos').addClass('active');
});
function ChamaRota(form, rota, retorno)
{
var xdata = $(form).serialize();
$(retorno).html('<div style="text-align:center"><i class="icon-spinner icon-spin icon-3x"></i> carregando...</div>');
$.ajax({
    type: 'POST'
    , url: rota
    , data: xdata
    , dataType: 'html'
    , success:
            function(data)
            {
                $(retorno).html(data);
            }
});
}

function ExcluirAnexo(id)
{
$.ajax({
    type: 'GET'
    , url: '/anexo/excluir/' + id
    , dataType: 'html'
    , success:
            function(data)
            {
                AtualizarAnexos();
            }
});

}

function AtualizaTipoAnexo(id, id_tipo_anexo)
{
$.ajax({
    type: 'POST'
    , url: '/anexo/atualizartipo/' + id
    , data: {"id_tipo_anexo": $(id_tipo_anexo).val()}
    , success:
            function(data)
            {
                AtualizarAnexos();
            }
});

}

function AtualizarAnexos()
{
$('#containerAnexos').html('<div style="text-align:center; padding:20px;"><i class="icon-spinner icon-spin icon-4x"></i></div>');

$.ajax({
    type: 'GET'
    , url: '/anexo/doproduto/' + $('#id_produto').val()
    , dataType: 'html'
    , success:
            function(data)
            {
                $('#containerAnexos').html(data);
            }
});

}

$(document).ready(function() {
    Dropzone.options.myAwesomeDropzone = {
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2000, // MB
        complete: function(file, done) {
            AtualizarAnexos();
        },
        init: function(){
            this.on("sucess", function(file, response) { alert(response); });
        }
    };
});


$("#nome_completo").keyup(function(e) {
if (e.keyCode == 13) {
    ChamaRota('#buscaCandidato', '/candidato/foradaos', '#candidatosEncontrados');
    return false;
}
});



</script>

@stop
