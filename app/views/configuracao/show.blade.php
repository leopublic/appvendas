@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Configurações da aplicação</h1>
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Valores cadastrados</h3>
            </div>

            <div class="box-content">
                {{ Form::open(array("url"=>"/configuracao/store", "class"=>"form-horizontal", "files"=> true)) }}
                    @include('html/campo-texto', array('id' => 'UnidadeNegocioID', 'label' => 'ID da unidade de negócios', 'valor' => $UnidadeNegocioID, 'help' => ''))
                    @include('html/campo-texto', array('id' => 'url_signus', 'label' => 'URL do Signus', 'valor' => $url_signus, 'help' => ''))
                    @include('html/campo-texto', array('id' => 'url_appvendas', 'label' => 'URL do Appvendas', 'valor' => $url_appvendas, 'help' => ''))
                    @include('html/campo-texto', array('id' => 'tamanho_capa', 'label' => 'Maior dimensão da capa', 'valor' => $tamanho_capa, 'help' => ''))
                    @include('html/campo-texto', array('id' => 'tamanho_thumb', 'label' => 'Maior dimensão da miniatura', 'valor' => $tamanho_thumb, 'help' => ''))
                    @include('html/campo-texto', array('id' => 'qualidade_jpeg', 'label' => 'Qualidade do JPEG', 'valor' => $qualidade_jpeg, 'help' => ''))
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                        </div>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@stop