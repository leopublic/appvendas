@if(count($anexos) > 0)
<table class="table table-striped table-hover fill-head">
    <thead>
        <tr>
            <th style="width: 40px;text-align: center;">#</th>
            <th style="width: 100px; text-align: center;">Ação</th>
            <th style="width: auto">Nome</th>
        </tr>
    </thead>
    <tbody>
        <? $i = 0; ?>
        @foreach($anexos as $anexo)
        <? $i++; ?>
        <tr>
            <td style="text-align: center;">{{ $i }}</td>
            <td style="text-align: center;">
                <a class="btn btn-primary" href="{{ URL::to('/anexo/visualizar/'.$anexo->id_anexo) }}"><i class="icon-play"></i></a>
                @include('html.link_exclusao', array('url' => '/anexo/del/'.$anexo->id_anexo, 'msg_confirmacao' => 'Clique para confirmar a exclusão desse vídeo', 'icon'=>'icon-trash'))
            </td>
            <td>{{ $anexo->nome_original}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
(nenhum vídeo encontrado)
@endif
