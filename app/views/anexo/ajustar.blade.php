@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i> Ajustar tamanhos das capas"</h1>
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Progresso</h3>
            </div>

            <div class="box-content">
                <div class="row">
    <div class="col-md-12">
                    <button class="btn btn-primary" onclick="iniciarProcessamento();"> Clique para iniciar o processamento</button>
                    </div>
                </div>
                <div class="row">
    <div class="col-md-12">
                    <div class="progress progress-striped active" id="containerBarra">
                        <div style="width: 0%;" class="progress-bar" id="barra"></div>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
var atualiza = function atualizar(){
    var url='/anexo/progresso';
    $.getJSON(url)
        .done(function(data) {
            console.log(data);
            var prog = data.progresso;
            $('#barra').css('width', prog+"%");
            if (data.processando == '0'){
                $('#containerBarra').removeClass("active");
                alert("Processamento concluído. "+data.alterados+"/"+data.processados+" arquivos alterados.<br/>Tamanho total reduzido de "+data.tam_antes+" para "+data.tam_depois);
            } else {
                setTimeout(atualiza, 2000);
            }
        });
}

function iniciarProcessamento(){
    var url='/anexo/ajustar';
    $.get(url)
        .done();
    atualiza();
}

</script>
@stop