@extends('base')

@section('content')
<!-- BEGIN Page Title -->
@if ($anexo->id_produto > 0)
<div class="page-title">
    <h1><i class="icon-folder-open"></i>{{$anexo->produto->artista}}: "{{$anexo->produto->titulo}}"</h1>
</div>
@else
<div class="page-title">
    <div>
        <h1><i class="icon-bullhorn"></i>Publicidade</h1>
    </div>
</div>
@endif
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Ver vídeo</h3>
            </div>

            <div class="box-content">
                <video controls>
                    <source type="{{$anexo->mime()}}" src="{{ URL::to($anexo->url()) }}">
                </video>
            </div>
        </div>
    </div>
</div>

@stop
