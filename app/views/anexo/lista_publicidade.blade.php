@extends('base')

@section('content')
	<div class="page-title">
	    <div>
	        <h1><i class="icon-bullhorn"></i>Publicidade</h1>
	    </div>
	</div>

@include('html/mensagens')

<!-- BEGIN Main Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-plus"></i> Adicionar vídeos</h3>
                </div>
                <div class="box-content">
                    <form action="/anexo/novapublicidade" class=" dropzone hidden-xs dz-clickable" id="my-awesome-dropzone" method="POST">
                        <div class="dz-message" style="text-align:center;height:">
                            Arraste os arquivos para cá ou clique para selecionar
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="icon-paper-clip"></i> Vídeos disponíveis</h3>
                </div>
                <div class="box-content" id="containerAnexos">
                    @include('anexo.index', array('anexos' => $anexos))
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script type="text/javascript" src="/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#menuPublicidade').addClass('active');
        
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 2000, // MB
            complete: function(file, done) {
                AtualizarAnexos();
            },
            init: function(){
                this.on("sucess", function(file, response) { alert(response); });
            }
        };
        
    });
</script>
@stop