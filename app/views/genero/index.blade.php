@extends('base')

@section('content')
	<div class="page-title">
	    <div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{URL::to('/genero/edite/0')}}" title="Clique para criar um novo gênero">Criar novo</a>
                </div>
	        <h1><i class="icon-tag"></i>Gêneros</h1>
	    </div>
	</div>

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Gêneros</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 30px; text-align: center;">#</th>
                            <th style="width: 180px; text-align: center;">Ação</th>
                            <th style="width: auto;">Nome</th>
                            <th style="width: 150px; text-align: center;">Cadastrado em</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $reg)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align: center;"><? print $i; ?></td>
                            <td style=" text-align: center;">
                                <a class="btn btn-primary" href="{{ URL::to('/genero/edite/'.$reg->id_genero) }}" title="Editar esse gênero"><i class="icon-pencil"></i></a>
                                @include('html.link_exclusao', array('url'=> '/genero/exclua/'.$reg->id_genero, 'msg_confirmacao' => 'Clique para confirmar a exclusão desse gênero', 'title' => 'Excluir'))
                            </td>
                            <td>{{ $reg->nome}}</td>
                            <td style="text-align: center;">{{ $reg->created_at_fmt}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript" src="/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="/assets/dropzone-3.8.3/downloads/dropzone.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#menuGeneros').addClass('active');
    });
</script>
@stop
