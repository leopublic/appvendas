@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Pedidos', 'subTitulo' => '', 'icon' => 'icon-shopping-cart'))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array("class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Filtrar por</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'titulo', 'label' => 'Título', 'valor' => Input::old('titulo'),  'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-texto', array('id' => 'artista', 'label' => 'Artista', 'valor' => Input::old('artista'),  'help' => '',  array()))
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'ClienteID', 'label' => 'Cliente', 'valores' => $clientes, 'valor' => Input::old('ClienteID'),  'help' => '',  array()))
                    </div>
                    <div class="col-md-6">
                        @include('html/campo-select', array('id' => 'id_usuario_vendedor', 'label' => 'Vendedor', 'valores' => $vendedores, 'valor' => Input::old('id_usuario_vendedor'),  'help' => '',  array()))
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close()}}
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Pedidos</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
            @if(isset($registros))
                {{$registros->links()}}
            @endif
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 180px">Ação</th>
                            <th style="width: 200px; text-align: left;">Vendedor</th>
                            <th style="width: auto;">Cliente</th>
                            <th style="width: auto; text-align: center;">Data</th>
                            <th style="width: auto; text-align: center;">Itens</th>
                            <th style="width: auto; text-align: center;">Enviado</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $reg)
                        <? $i++; ?>
                        <tr>
                            <td><? print $i; ?></td>
                            <td>
                                <a class="btn btn-primary" href="{{ URL::to('/pedido/edit/'.$reg->id_pedido) }}" title="Detalhes do pedido"><i class="icon-pencil"></i></a>
                            </td>
                            <td style="text-align: center;">{{ $reg->vendedor->fantasia }}</td>
                            <td>{{ $reg->cliente->razao_social }}</td>
                            <td>{{ $reg->dt_pedido_fmt }}</td>
                            <td style="text-align: center;">{{ $reg->qtd_itens}}</td>
                            <td style="text-align: center;">{{ $reg->status_envio_detalhado}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop


@section('scripts')
<script type="text/javascript">

$(document).ready(function(){
    $('#menuProdutos').addClass('active');
});
</script>
@stop
