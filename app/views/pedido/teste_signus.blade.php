@extends('base')

@section('content')
@include('html/page-title', array('titulo' => 'Pedidos', 'subTitulo' => '', 'icon' => 'icon-shopping-cart'))

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        {{ Form::open(array("class"=>"form-horizontal")) }}
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Teste envio</h3>
                <div class="box-tool">
                    <button type="submit"><i class="icon-search"></i></button>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        @include('html/campo-texto', array('id' => 'url', 'label' => 'Url', 'valor' => 'http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc',  'help' => '',  array()))
                    </div>
                    <div class="col-md-12">
                        @include('html/campo-texto', array('id' => 'metodo', 'label' => 'Metodo', 'valor' => Input::old('metodo'),  'help' => '',  array()))
                    </div>
                    <div class="col-md-12">
                        @include('html/campo-texto', array('id' => 'parametros', 'label' => 'Parametros', 'valor' => Input::old('metodo'),  'help' => '',  array()))
                    </div>
                    <div class="col-md-12">
                        @include('html/campo-textarea', array('id' => 'resultado', 'label' => 'Resultado', 'valor' => Input::old('resultado'),  'help' => '',  array()))
                    </div>
                </div>
                <div class="row">
                    <button type='button' class='btn' onclick='envia();'>Testar</button>
                </div>
                
            </div>
        </div>
        {{ Form::close()}}
    </div>
</div>
@stop
@section('scripts')
<script type="text/javascript">

function envia(){
    var metodo = $('#metodo');
    var dados = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://tempuri.org/">  <SOAP-ENV:Body>    <ns1:ObterClientes>      <ns1:strXML><![CDATA[<?xml version="1.0"?><Solicitacao><VendedorID>6</VendedorID><UnidadeNegocioID>15</UnidadeNegocioID></Solicitacao>]]></ns1:strXML></ns1:ObterClientes></SOAP-ENV:Body></SOAP-ENV:Envelope>';
    $.ajax({
        url: 'http://189.44.180.59/SIGNUSERPIntegracaoCMSM2/WSIntegracaoCMS.svc?wsdl',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: dados,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
          alert(textStatus);
          $('#resultado').html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert(textStatus);
          console.log(errorThrown);
          console.log(textStatus);
          $('#resultado').html(data);
        },
    });
}
</script>
@stop
