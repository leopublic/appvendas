@extends('base')

@section('content')
<!-- BEGIN Page Title -->
<div class="page-title">
    <h1><i class="icon-folder-open"></i>Versões do aplicativo Android"</h1>
</div>
<!-- END Page Title -->

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Adicionar versão</h3>
            </div>

            <div class="box-content">
                {{ Form::open(array("url"=>"/versao/store", "class"=>"form-horizontal", "files"=> true)) }}
                {{Form::hidden('id_versao', 0)}}
                    @include('html/campo-texto', array('id' => 'num_major', 'label' => 'Major', 'valor' => '', 'help' => ''))
                    @include('html/campo-texto', array('id' => 'num_minor', 'label' => 'Minor', 'valor' => '', 'help' => ''))
                    @include('html/campo-texto', array('id' => 'num_patch', 'label' => 'Patch', 'valor' => '', 'help' => ''))
                    @include('html/campo-textarea', array('id' => 'observacao', 'label' => 'Observação', 'valor' => '', 'help' => ''))
                    <div class="form-group">
                        <div class="controls">
                            <label class="col-sm-3 col-lg-2 control-label">Apk</label>
                            <div class="col-sm9 col-lg-10 controls">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-group">
                                        <div class="form-control uneditable-input">
                                            <i class="icon-file fileupload-exists"></i> 
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <div class="input-group-btn">
                                            <a class="btn bun-default btn-file">
                                                <span class="fileupload-new">Selecione o arquivo</span>
                                                <span class="fileupload-exists">Alterar</span>
                                                <input type="file" name="versao" class="file-input"/>
                                            </a>
                                            <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Cancelar</a>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><i class="icon-cloud-upload"></i> Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                            </div>
                        </div>
                    {{Form::close()}}
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-folder-open"></i>Adicionar versão</h3>
            </div>

            <div class="box-content">
                @if(is_object($versoes ))
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 150px; text-align: center;">Ações</th>
                            <th style="width: auto">Número</th>
                            <th style="width: auto;">Data</th>
                            <th style="width: auto;">Observação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($versoes as $versao)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align: center;">
                                @if ($versao->fl_atual == 1)
                                <a href="#" title="Essa é a versão atual" class="btn btn-default"><i class="icon-exclamation"></i></a>                                
                                @else
                                <a href="{{URL::to('versao/tornaratual/'.$versao->id_versao)}}" title="Tornar essa versão a versão atual" class="btn btn-warning"><i class="icon-exclamation"></i></a>
                                @endif
                                <a href="{{URL::to('versao/download/'.$versao->id_versao)}}" target="_blank" title="Download dessa versao" class="btn btn-primary"><i class="icon-download"></i></a>
                                @include('html.link_exclusao', array('url'=> '/versao/del/'.$versao->id_versao, 'msg_confirmacao' => 'Clique para confirmar a exclusão dessa versão', 'title' => 'Excluir', 'icon'=>'icon-trash'))
                            </td>
                            <td>{{ $versao->numero }}</td>
                            <td>{{ $versao->dt_disponivel_fmt }}</td>
                            <td>{{ nl2br($versao->observacao) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                (nenhuma versão encontrada)
                @endif
            </div>
        </div>
    </div>
</div>

@stop
@section('scripts')
<script type="text/javascript" src="/assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
    $('#menuTodasVersoes').addClass('active');
});
</script>

@stop