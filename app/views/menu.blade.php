<!-- BEGIN Navlist -->
<ul class="nav nav-list">
    <!-- BEGIN Search Form -->
    <!--                    <li>
                            <form target="#" method="GET" class="search-form">
                                <span class="search-pan">
                                    <button type="submit">
                                        <i class="icon-search"></i>
                                    </button>
                                    <input type="text" name="search" placeholder="Search ..." autocomplete="off" />
                                </span>
                            </form>
                        </li>-->
    <!-- END Search Form -->
    <li id="menuHome"><a href="{{ URL::to('/') }}"><i class="glyphicon glyphicon-home"></i><span>Home</span></a></li>
    <li id="menuFamilias"><a href="{{ URL::to('familia') }}"><i class="icon-link"></i><span>Famílias</span></a></li>
    <li id="menuGeneros"><a href="{{ URL::to('genero') }}"><i class="icon-tag"></i><span>Gêneros</span></a></li>
    <li id="menuProdutos"><a href="{{ URL::to('produto') }}"><i class="icon-music"></i><span>Produtos</span></a></li>
    <li id="menuProdutosBloqueados"><a href="{{ URL::to('produto/novos') }}"><i class="icon-lock"></i><span>Produtos bloqueados</span></a></li>
    <li id="menuPublicidade"><a href="{{ URL::to('anexo/publicidade') }}"><i class="icon-bullhorn"></i><span>Publicidade</span></a></li>
    <li class="divider">&nbsp;</li>
    <li id="menuPedidos"><a href="{{ URL::to('pedido') }}"><i class="icon-shopping-cart"></i><span>Pedidos</span></a></li>
    <li class="divider">&nbsp;</li>
    <li id="menuVersaoAtual"><a href="{{ URL::to('versao/atual') }}" target="_blank"><i class="icon-android"></i><span>Versão atual do app</span></a></li>
    <li id="menuImportarXML"><a href="{{ URL::to('produto/carregaxml') }}" target="_blank"><i class="icon-upload"></i><span>Importar produtos</span></a></li>
    <li id="menuTodasVersoes"><a href="{{ URL::to('versao/show') }}"><i class="icon-calendar"></i><span>Todas versões</span></a></li>
    <li class="divider">&nbsp;</li>
    <li id="menuCorrigir"><a href="{{ URL::to('anexo/acompanhaajuste') }}"><i class="icon-magic"></i><span>Ajustar imagens</span></a></li>
    <li class="divider">&nbsp;</li>
    <li id="menuTodasVersoes"><a href="{{ URL::to('configuracao/show') }}"><i class="icon-cogs"></i><span>Configurações</span></a></li>
    <li class="divider">&nbsp;</li>
    <li>
        <a href="#" class="dropdown-toggle">
            <i class="icon-key"></i><span>Controle de acesso</span>
            <b class="arrow icon-angle-right"></b>
        </a>

        <!-- BEGIN Submenu -->
        <ul class="submenu">
            <li><a href="{{ URL::to('perfil') }}">Perfis</a></li>
            <li><a href="{{ URL::to('usuario') }}">Usuários</a></li>
        </ul>
        <!-- END Submenu -->
    </li>

</ul>
<!-- END Navlist -->
