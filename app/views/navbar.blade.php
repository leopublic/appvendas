        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar navbar-gray">
            <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="icon-reorder"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('/') }}">
                <small>
                    <i class="icon-check-sign"></i>
                    Som Livre - Suporte à Vendas
                </small>
            </a>

            <!-- BEGIN Navbar Buttons -->
            <ul class="nav flaty-nav pull-right">
                <li class="user-profile">
                    <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        <span class="hidden-sm" id="user_info">{{\Auth::user()->nome}}
                        </span>
                        <i class="icon-caret-down"></i>
                    </a>

                    <!-- BEGIN User Dropdown -->
                    <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                        <li>
                            <a href="{{ URL::to('usuario/altereminhasenha/')}}">
                                <i class="icon-key"></i>
                                Alterar senha
                            </a>
                        </li>


                        <li class="divider"></li>

                        <li>
                            <a href="{{ URL::to('auth/logout')}}">
                                <i class="icon-off"></i>
                                Sair
                            </a>
                        </li>
                    </ul>
                    <!-- BEGIN User Dropdown -->
                </li>
                <!-- END Button User -->
            </ul>
            <!-- END Navbar Buttons -->
        </div>
        <!-- END Navbar -->