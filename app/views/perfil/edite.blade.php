@extends('base')

@section('content')
<div class="page-title">
    <div>
        <h1><i class="icon-group"></i>
            @if ($model->id_perfil> 0)
            Alterar perfil de acesso
            @else
            Criar novo perfil de acesso
            @endif
        </h1>
    </div>
</div>
@include('html/mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-pencil"></i> 
                    @if ($model->id_perfil> 0)
                    {{$model->descricao}}
                    @else
                    (novo)
                    @endif
                </h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                {{ Form::hidden('id_perfil', $model->id_perfil) }}
                @include('html/campo-texto', array('id' => 'descricao', 'label' => 'Descrição', 'valor' => $model->descricao,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $model->observacao,  'help' => '',  array()))

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

</div>
@stop