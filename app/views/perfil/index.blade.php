@extends('base')

@section('content')
	<div class="page-title">
	    <div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{URL::to('/perfil/edite/0')}}" title="Clique para criar um novo usuário">Criar novo</a>
                </div>
	        <h1><i class="icon-group"></i>Perfis de acesso</h1>
	    </div>
	</div>

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Perfis</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 30px; text-align: center;">#</th>
                            <th style="width: 150px; text-align: center;">Ação</th>
                            <th style="width: auto;">Descrição</th>
                            <th style="width: auto;">Observação</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $reg)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align: center;"><? print $i; ?></td>
                            <td style=" text-align: center;">
                                <a class="btn btn-primary" href="{{ URL::to('/perfil/edite/'.$reg->id_perfil) }}" title="Editar perfil"><i class="icon-pencil"></i></a>
                                @include('html.link_exclusao', array('url'=> '/perfil/exclua/'.$reg->id_perfil, 'msg_confirmacao' => 'Clique para confirmar a exclusão desse perfil', 'title' => 'Excluir'))
                            </td>
                            <td>{{ $reg->descricao}}</td>
                            <td>{{ $reg->observacao}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop