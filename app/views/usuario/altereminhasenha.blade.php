@extends('base')

@section('content')
<!-- BEGIN Page Title -->
@include('html/page-title', array('titulo' => 'Alterar minha senha', 'subTitulo' => ''))
@include('html/mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-reorder"></i> Informe a senha atual e a nova</h3>
            </div>
            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                @include('html/campo-password', array('id' => 'atual', 'label' => 'Senha atual', 'valor' => '',  'help' => '',  array()))
                @include('html/campo-password', array('id' => 'password', 'label' => 'Nova senha', 'valor' => '',  'help' => '',  array()))
                @include('html/campo-password', array('id' => 'password_confirmation', 'label' => '(repita)', 'valor' => '',  'help' => '',  array()))
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Alterar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

</div>
@stop