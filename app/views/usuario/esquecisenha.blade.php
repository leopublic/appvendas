<!DOCTYPE html>
<html>
    <head>
        @include('html/head')
        @yield('styles')
    </head>
    <body class="login-page">
        <!-- BEGIN Main Content -->
        <div class="login-wrapper">
            <!-- BEGIN Login Form -->
            {{ Form::open(array('style'=>'width:400px;')) }}
                <h3>Som Livre - Representantes</h3>
                <hr/>
                <div class="form-group">
                    Informe o e-mail do seu cadastro no sistema
                </div>
                <div class="form-group">
                    <div class="controls">
                        <input type="email" name="email" placeholder="e-mail" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary form-control">Enviar</button>
                    </div>
                </div>
                <hr/>

                @if (Session::has('login_errors'))
                    <span class="error">{{ Session::get('msg') }}</span>
                @endif
            {{ Form::close() }}
            </form>
            <!-- END Login Form -->
        </div>
        <!-- END Main Content -->

        <!--basic scripts-->
        <script src="assets/jquery/jquery-2.0.3.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

    </body>
</html>
