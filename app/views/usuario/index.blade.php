@extends('base')

@section('content')
	<div class="page-title">
	    <div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{URL::to('/usuario/edite/0')}}" title="Clique para criar um novo usuário">Criar novo</a>
                </div>
	        <h1><i class="icon-male"></i>Usuários</h1>
	    </div>
	</div>

@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-table"></i>Usuários</h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-hover fill-head">
                    <thead>
                        <tr>
                            <th style="width: 30px; text-align: center;">#</th>
                            <th style="width: 220px; text-align: center;">Ação</th>
                            <th style="width: auto;">Nome</th>
                            <th style="width: auto;">E-mail (login)</th>
                            <th style="width: auto;">Observação</th>
                            <th style="width: 150px; text-align: center;">Cadastrado em</th>
                            <th style="width: 150px; text-align: center;">Último<br/>acesso</th>
                            <th style="width: 150px; text-align: center;">Última<br/>sincronização</th>
                        </tr>
                    </thead>
                    @if(isset($registros))
                    <tbody>
                        <? $i = 0; ?>
                        @foreach($registros as $reg)
                        <? $i++; ?>
                        <tr>
                            <td style="text-align: center;"><? print $i; ?></td>
                            <td style=" text-align: center;">
                                <a class="btn btn-primary" href="{{ URL::to('/usuario/edite/'.$reg->id_usuario) }}" title="Editar usuário"><i class="icon-pencil"></i></a>
                                <a class="btn btn-warning" href="{{ URL::to('/usuario/alteresenha/'.$reg->id_usuario) }}" title="Gerar nova senha"><i class="icon-key"></i></a>
                                @if ($reg->email != '')
                                <a class="btn btn-lime" href="{{ URL::to('/usuario/envienovasenha/'.$reg->id_usuario) }}" title="Enviar uma nova senha randômica por e-mail"><i class="icon-envelope"></i></a>
                                @else
                                <a class="btn" href="javascript:var n = noty({theme: 'relax', layout: 'center',  timeout: 3000, type: 'error', text: 'Esse usuário não possui email cadastrado!'});" title="Esse usuário não possui email cadastrado"><i class="icon-envelope"></i></a>
                                @endif
                                @include('html.link_exclusao', array('url'=> '/usuario/exclua/'.$reg->id_usuario, 'msg_confirmacao' => 'Clique para confirmar a exclusão desse usuário', 'title' => 'Excluir'))
                            </td>
                            <td>{{ $reg->nome}}</td>
                            <td>{{ $reg->email}}</td>
                            <td>{{ $reg->observacao}}</td>
                            <td style="text-align: center;">{{ $reg->created_at_fmt}}</td>
                            <td style="text-align: center;">{{ $reg->dt_ultimo_acesso_fmt }}</td>
                            <td style="text-align: center;">{{ $reg->dt_ultima_sincronizacao_fmt }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
@stop