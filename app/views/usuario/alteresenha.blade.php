@extends('base')

@section('content')
<div class="page-title">
    <div>
        <h1><i class="icon-key"></i>Alterar senha do usuário "{{ $model->nome }}"</h1>
    </div>
</div>
@include('html/mensagens')

<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-reorder"></i> Alterar senha </h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                <input type="hidden" name="id_usuario" value="{{$model->id_usuario}}"/>
                @include('html/campo-password', array('id' => 'password', 'label' => 'Senha', 'valor' => '',  'help' => '',  array()))
                @include('html/campo-password', array('id' => 'password_confirmation', 'label' => '(repita)', 'valor' => '',  'help' => '',  array()))
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@stop