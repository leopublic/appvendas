@extends('base')

@section('content')
<div class="page-title">
    <div>
        <h1><i class="icon-male"></i>
            @if ($model->id_usuario > 0)
            Alterar usuário
            @else
            Criar usuário
            @endif
        </h1>
    </div>
</div>
@include('html/mensagens')
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="icon-pencil"></i> 
                    @if ($model->id_usuario > 0)
                    {{$model->nome}}
                    @else
                    (novo)
                    @endif
                </h3>
                <div class="box-tool">
                </div>
            </div>
            <div class="box-content">
                {{ Form::open(array("class"=>"form-horizontal")) }}
                {{ Form::hidden('id_usuario', $model->id_usuario) }}
                @include('html/campo-texto', array('id' => 'nome', 'label' => 'Nome', 'valor' => $model->nome,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'email', 'label' => 'Email (login)', 'valor' => $model->email,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'signus_id', 'label' => 'ID Signus', 'valor' => $model->signus_id,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'codigo', 'label' => 'Código Signus', 'valor' => $model->codigo,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'razao_social', 'label' => 'Razão social', 'valor' => $model->razao_social,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'fantasia', 'label' => 'Fantasia', 'valor' => $model->fantasia,  'help' => '',  array()))
                @include('html/campo-texto', array('id' => 'observacao', 'label' => 'Observação', 'valor' => $model->observacao,  'help' => '',  array()))

                <div class="form-group">
                    {{Form::label('id_perfil', 'Perfis do usuário', array('class' => 'col-sm-3 col-lg-2 control-label')) }}
                    <div class="col-sm-9 col-lg-10 controls">
                        <div class="row">
                            @foreach(Perfil::orderBy('descricao')->get() as $perfil)
                            <div class="col-md-3">
                                <input type="checkbox" name="id_perfil[]" value="{{$perfil->id_perfil}}" title="{{$perfil->observacao}}"
                                       @if (in_array($perfil->id_perfil, $model->arrayDePerfis() ) )
                                       checked="checked"
                                       @endif
                                       /> {{$perfil->descricao}}
                            </div>
                            @endforeach
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Salvar</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

</div>
@stop