<!DOCTYPE html>
<html>
    <head>
        @include('html/head')
        @yield('styles')
    </head>
    <body class="login-page">
        <!-- BEGIN Main Content -->
        <div class="login-wrapper">
            <!-- BEGIN Login Form -->
            {{ Form::open(array('style'=>'width:400px;')) }}
                <h3>Som Livre - Representantes</h3>
                <hr/>
                <div class="form-group">
                    <div class="controls">
                        <input type="text" name="email" placeholder="Login" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <input type="password" name="password" placeholder="Senha" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary form-control">Entrar</button>
                    </div>
                </div>
                <hr/>
                @if (Session::has('login_errors'))
                    <span class="error">{{ Session::get('msg') }}</span><br/>
                @endif
                <a href="/auth/esquecisenha">Esqueci minha senha!</a>
            {{ Form::close() }}
            </form>
            <!-- END Login Form -->
        </div>
        <!-- END Main Content -->

        <!--basic scripts-->
        <script src="assets/jquery/jquery-2.0.3.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

        <script type="text/javascript">
            function goToForm(form)
            {
                $('.login-wrapper > form:visible').fadeOut(500, function(){
                    $('#form-' + form).fadeIn(500);
                });
            }
            $(function() {
                $('.goto-login').click(function(){
                    goToForm('login');
                });
                $('.goto-forgot').click(function(){
                    goToForm('forgot');
                });
                $('.goto-register').click(function(){
                    goToForm('register');
                });
            });
        </script>
    </body>
</html>
