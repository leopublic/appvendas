<!DOCTYPE html>
<html>
    @include('html/head')
    <body class="skin-black">

	@include('navbar')

        <!-- BEGIN Container -->
        <div class="container" id="main-container">
            <!-- BEGIN Sidebar -->
            <div id="sidebar" class="navbar-collapse collapse">
				@include('menu')
                <!-- BEGIN Sidebar Collapse Button -->
                <div id="sidebar-collapse" class="visible-lg">
                    <i class="icon-double-angle-left"></i>
                </div>
                <!-- END Sidebar Collapse Button -->
            </div>
            <!-- END Sidebar -->
            <div id="main-content">
            <!-- BEGIN Content -->
			@yield ('content')
                <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="icon-chevron-up"></i></a>
            </div>
            <!-- END Content -->
        </div>
        <!-- END Container -->
		<div id="footer">
			<div class="container">
                    <p>2013 © M2 Software.</p>
			</div>
		</div>

        <!--basic scripts-->
        <script src="/assets/jquery/jquery-2.0.3.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
        <script src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/assets/nicescroll/jquery.nicescroll.min.js"></script>
        <script src="/assets/jquery-cookie/jquery.cookie.js"></script>        <!--page specific plugin scripts-->
        <script src="/assets/flot/jquery.flot.js"></script>
        <script src="/assets/flot/jquery.flot.resize.js"></script>
        <script src="/assets/flot/jquery.flot.pie.js"></script>
        <script src="/assets/flot/jquery.flot.stack.js"></script>
        <script src="/assets/flot/jquery.flot.crosshair.js"></script>
        <script src="/assets/flot/jquery.flot.tooltip.min.js"></script>
        <script src="/assets/sparkline/jquery.sparkline.min.js"></script>

        <!--flaty scripts-->
        <script src="/js/flaty.js"></script>
        @yield ('scripts')
    </body>
</html>
