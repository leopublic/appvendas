<?php
Route::controller('auth', 'AuthController');
Route::controller('sinc', 'SincronizacaoController');

Route::group(array('before' => 'autenticado'), function() {
    Route::get('/', function() {
        return View::make('hello');
    });

    Route::controller('anexo', 'AnexoController');
    Route::controller('configuracao', 'ConfiguracaoController');
    Route::controller('evento', 'EventoController');
    Route::controller('familia', 'FamiliaController');
    Route::controller('genero', 'GeneroController');
    Route::controller('musica', 'MusicaController');
    Route::controller('pedido', 'PedidoController');
    Route::controller('perfil', 'PerfilController');
    Route::controller('produto', 'ProdutoController');
    Route::controller('usuario', 'UsuarioController');
    Route::controller('versao', 'VersaoController');

});
