<?php
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AppvendasGeraSenha extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'appvendas:gera-senha';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Renova a senha de um usuário.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
		$email = $this->argument('email');
        $usuario = \Usuario::where('email', '=', $email)->first();
        if (count($usuario) == 0){
        	$this->error('Nenhum usuário com esse e-mail encontrado. Verifique.');
        } else {
			$senha = $this->argument('senha');
        	if (trim($senha) != ''){
	            $usuario->password = \Hash::make($senha);
	            $usuario->save();
        		$this->info('Senha alterada com sucesso.');
        	} else {
        		$this->error('Senha não informada. Verifique.');
        	}
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
        		array('email', InputArgument::REQUIRED, 'Email do usuário')
        		, array('senha', InputArgument::REQUIRED, 'Nova senha')
        	);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array();
    }
}
