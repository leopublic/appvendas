<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AdminImportarAcervo extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'appvendas:importar-acervo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar acervo existente.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        $caminho_arquivo = $this->option('caminho_arquivo');
        $caminho_media_destaque = $this->option('caminho_media_destaque');
        $caminho_media_thumb = $this->option('caminho_media_thumb');
        $nome_arquivo = $this->option('nome_arquivo');
        if (File::exists($caminho_arquivo.$nome_arquivo)){
            $arquivo = File::get($caminho_arquivo.$nome_arquivo);
            $linhas = explode("\n", $arquivo);
            $serv = new \Appvendas\Servicos\Carga;
            $i = 1;
            foreach($linhas as $linha){
                $colunas = explode("\t", $linha);
                try{
                    $serv->carregar($caminho_media_destaque, $caminho_media_thumb, $colunas);
                    $this->info("Linha ".$i." => Catálogo ".$colunas[0]." importado com sucesso.");
                } catch (Exception $ex) {
                    $this->error("Linha ".$i." => Catálogo ".$colunas[0]." com erro:".$ex->getMessage());
                }
                $i++;
            }
        } else {
            $this->info("Arquivo ".$caminho_arquivo.$nome_arquivo." não encontrado. Importação não realizada");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('caminho_arquivo', 'caminho_arquivo', InputOption::VALUE_OPTIONAL, 'Caminho do arquivo de importação.', Config::get('appvendas.caminho_arquivo_importacao')),
            array('caminho_media_destaque', 'caminho_media_destaque', InputOption::VALUE_OPTIONAL, 'Raiz dos arquivos de midia (capa destaque).', Config::get('appvendas.caminho_media_importacao_destaque')),
            array('caminho_media_thumb', 'caminho_media_thumb', InputOption::VALUE_OPTIONAL, 'Raiz dos arquivos de midia (capa thumb).', Config::get('appvendas.caminho_media_importacao_thumb')),
            array('nome_arquivo', 'nome_arquivo', InputOption::VALUE_OPTIONAL, 'Caminho do arquivo de importação.', "carga_appvendas.txt"),
        );
    }

}
