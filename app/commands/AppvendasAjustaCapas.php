<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AppvendasAjustaCapas extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'appvendas:ajusta-capas';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Ajusta o tamanho de todas as capas do sistema';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        set_time_limit(0);
        $rep = new \Appvendas\Repositorios\RepositorioAnexo;
        $produtos = \Produto::all();
        $tamCapa = \Configuracao::tamanho_capa();
        $tamThumb = \Configuracao::tamanho_thumb();
        $qualidade = \Configuracao::qualidade_jpeg();
        $qualidade = 100;

        $qtdTotal = \Produto::all()->count();
        $qtdTotal = $qtdTotal * 2;
        $processados = 0;
        $tam_antes = 0;
        $tam_depois = 0;
        $alterados = 0;

        try {
            foreach ($produtos as $produto) {
            	try{
            		$this->info('Convertendo produto (ID='.$produto->id_produto.') '.$produto->titulo);
	                // Converte capa
	                $arq = $produto->caminhoCapa();
	                if (file_exists($arq)) {
				        $tam_a = filesize($arq);
				        if ($tam_a > 0){
				        	$tam_antes += $tam_a;
	                    	$rep->ajustarImagem($arq, $tamCapa, $qualidade);
					        $tam_d = filesize($arq);
					        $tam_depois += $tam_d;
					        if ($tam_a != $tam_d){
					        	$alterados++;
					        }
					        $this->info('   => Capa alterada de '.$tam_a.' para '.$tam_d);
				        }
	                } else {
	              		$this->error('   => Capa não encontrada ');
	                }

	                // Converte thumb
	                $arq = $produto->caminhoThumb();
	                if (file_exists($arq)) {
				        $tam_a = filesize($arq);
				        if ($tam_a > 0){
				        	$tam_antes += $tam_a;
	                    	$rep->ajustarImagem($arq, $tamThumb, $qualidade);
					        $tam_d = filesize($arq);
					        $tam_depois += $tam_d;
					        if ($tam_a != $tam_d){
					        	$alterados++;
					        }
					        $this->info('   => Thumb alterada de '.$tam_a.' para '.$tam_d);
				        }
	                } else {
	              		$this->error('   => Thumb não encontrada ');
	                }
            	}
            	catch (Exception $e){
	              	$this->error('Erro: '.$e->getMessage());
            	}
	            $processados+=2;
            }
        }
        catch(Exception $e) {
	        $this->error('Erro: '.$e->getMessage());
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
