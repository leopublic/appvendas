<?php

/**
 * Na verdade os anexos são os vídeos
 */
class Anexo extends Eloquent {

    protected $table = 'anexo';
    protected $primaryKey = 'id_anexo';
    protected $guarded = array();

    const tpVIDEO = 2;

    public static function raiz() {
        return '/arquivos/anexos/';
    }

    public function caminho() {
        if ($this->id_tipo == self::tpVIDEO) {
            if ($this->id_produto > 0) {
                $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . Produto::SUBDIR_VIDEO;
            } else {
                $dir = public_path() . Config::get('appvendas.caminho_media') . "/publicidade/" . Produto::SUBDIR_VIDEO;
            }
            if (!is_dir($dir)){
                mkdir($dir, 0775, true);
            }
            return $dir . $this->id_anexo . "." . $this->extensao;
        }
    }

    public function url() {

        if ($this->id_produto > 0) {
            return URL::to(Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . Produto::SUBDIR_VIDEO . $this->id_anexo . "." . $this->extensao);
        } else {
            return URL::to(Config::get('appvendas.caminho_media') . "/publicidade/" . Produto::SUBDIR_VIDEO . $this->id_anexo . "." . $this->extensao);
        }
    }

    public function mime() {
        switch (strtolower($this->extensao)) {
            case 'bmp':
                return 'image/x-windows-bmp';
            case 'jpg':
                return 'image/jpeg';
            case 'png':
                return 'image/png';
            case 'gif':
                return 'image/gif';
            case 'tiff':
                return 'image/tiff';
            case 'pdf':
                return 'application/pdf';
            case 'doc':
                return 'application/msword';
            case 'docx':
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            case 'xls':
                return 'application/vnd.ms-excel';
            case 'xlsx':
                return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
            case 'ppt':
                return 'application/vnd.ms-powerpoint';
            case 'pptx':
                return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
            case 'avi':
                return 'video/avi';
            case 'mov':
                return 'video/quicktime';
            case 'mpeg':
                return 'video/mpeg';
            case 'mpg':
                return 'video/mpeg';
            case 'mp4':
                return 'video/mp4';
        }
    }

    public function tamanho() {
        filesize($this->caminho());
    }

    public function produto() {
        return $this->belongsTo('Produto', 'id_produto');
    }

}
