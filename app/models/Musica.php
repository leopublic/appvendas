<?php

class Musica extends Eloquent {
    protected $table = 'musica';
    protected $primaryKey = 'id_musica';

    protected $guarded = array();
    
    public function raiz() {
        $raiz =  public_path().Config::get('appvendas.caminho_media')."/".$this->id_produto."/".Produto::SUBDIR_AUDIO;
        if (!is_dir($raiz)){
            mkdir($raiz, 0775, true);
        }
        return $raiz;
    }

    public function caminho(){
        $caminho = $this->raiz().$this->id_musica.".".$this->extensao;
        return $caminho;
    }

    public function url() {
        return URL::to(Config::get('appvendas.caminho_media')."/".$this->id_produto."/".Produto::SUBDIR_AUDIO.$this->id_musica.".".$this->extensao );
    }

    public function tamanho() {
        filesize($this->caminho());
    }
    
    public function tem_trecho(){
        if (File::exists($this->caminho())){
            return true;
        } else {
            return false;
        }
    }
}
