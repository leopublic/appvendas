<?php

class Modelo extends Eloquent {

    public $errors;

    // public static function boot()
    // {
    // 	parent::boot();
    // 	static::saving(function($modelo){
    // 		$modelo->assumeDefaults();
    // 		return $modelo->valido($modelo->regras(), $modelo->mensagens());
    // 	});
    // 	static::saved(function($modelo){
    // 		// registra log
    // 	});
    // }

    public function assumeDefaults() {

    }

    public function valido($regras, $mensagens) {
        $validador = Validator::make($this->attributes, $regras, $mensagens);
        if ($validador->passes()) {
            return true;
        } else {
            $this->errors = $validador->messages();
            return false;
        }
    }

    public function dtModelParaView($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('Y-m-d', $data)->format('d/m/Y');
        } else {
            return null;
        }
    }

    public function dtHrModelParaView($data) {
        if ($data != '' ) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $data)->format('d/m/Y H:i:s');
        } else {
            return $data;
        }
    }

    public function dtViewParaModel($data) {
        if ($data != '' && $data != '00/00/0000' && $data != '0000-00-00') {
            return Carbon::createFromFormat('d/m/Y', substr($data, 0,10))->format('Y-m-d');
        } else {
            return null;
        }
    }

}
