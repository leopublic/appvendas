<?php

class Genero extends Modelo {
    protected $table = 'genero';
    protected $primaryKey = 'id_genero';

    protected $guarded = array();
 
    public function getCreatedAtFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['created_at']);
    }
   
}