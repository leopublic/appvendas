<?php
class Evento extends Modelo {
	protected $table = 'evento';
    protected $primaryKey = 'id_evento';
    protected $guarded = array();

    public function getDtAgendadaFmtAttribute($data) {
        if (isset($this->attributes['dt_agendada'])){
            return $this->dtModelParaView($this->attributes['dt_agendada']);
        } else {
            return '';
        }
    }

    public function setDtAgendadaFmtAttribute($data) {
        $this->attributes['dt_agendada'] = $this->dtViewParaModel($data);
    }

}

