<?php

class Extensao extends Modelo {

    protected $table = 'extensao';
    protected $primaryKey = 'id_extensao';
    protected $guarded = array();

    public function assumeDefaults() {

    }

    public static function regras() {
        return array(
        );
    }

    public static function mensagens() {
        return array(
        );
    }

    public function scopeFindPelaExtensao($query, $extensao) {
        return $query->where('extensao', '=', $extensao)->first();
    }

}
