<?php

class Trecho extends Eloquent {

    protected $table = 'trecho';
    protected $primaryKey = 'id_trecho';
    protected $guarded = array();

    public static function raiz() {
        return '/arquivos/trecho/';
    }

    public function extensao(){
        return $this->belongsTo('Extensao', 'id_extensao');
    }

    public function caminho() {
        return '../public' . $this->raiz() . $this->id_anexo;
    }

    public function url() {
        return URL::to($this->raiz() . $this->id_anexo);
    }

    public function tamanho() {
        filesize($this->caminho());
    }

}
