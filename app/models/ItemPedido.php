<?php

class ItemPedido extends Modelo {
    protected $table = 'item_pedido';
    protected $primaryKey = 'id_item_pedido';

    protected $guarded = array();
   
    public function pedido(){
        return $this->belongsTo('Pedido', 'id_pedido');
    }
    

    public function getPrecoBrutoAttribute($val){
        return $this->attributes['preco'];
        
    }

    public function getPrecoLiquidoAttribute($val){
        return $this->attributes['preco'];
    }
    
}