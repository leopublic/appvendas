<?php

class Imagem  {

    public static function raiz() {
        return '/arquivos/imagens/';
    }

    public function extensao(){
        return $this->belongsTo('Extensao', 'id_extensao');
    }

    public function caminho() {
        return '../public' . $this->raiz() . $this->id_anexo;
    }

    public function url() {
        return URL::to($this->raiz() . $this->id_anexo);
    }

    public function tamanho() {
        filesize($this->caminho());
    }

    public function produto() {
        return $this->belongsTo('Produto', 'id_produto');
    }
    
}
