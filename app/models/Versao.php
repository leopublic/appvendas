<?php
/**
 * @property int $id_versao 
 * @property smallint $num_major 
 * @property smallint $num_minor 
 * @property smallint $num_patch 
 * @property datetime $dt_disponivel 
 * @property text $observacao 
 * @property tinyint $fl_atual 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class Versao extends Modelo{

    protected $table = 'versaoapp';
    protected $primaryKey = 'id_versao';
    protected $guarded = array();

    public function caminho() {
        return app_path()."/android_app/appvendas_".$this->num_major."_".$this->num_minor."_".$this->num_patch.".apk";
    }
    
    public function getNumeroAttribute($valor){
        return substr('00'.$this->num_major, -2).".".substr('00'.$this->num_minor, -2).".".substr('00'.$this->num_patch, -2);
    }
    
    public function getDtDisponivelFmtAttribute($valor){
        return $this->dtHrModelParaView($this->attributes['dt_disponivel']);
    }
    
    public function getNomeAttribute($valor){
        return "appvendas.apk";
    }
    
    public function tamanho() {
        filesize($this->caminho());
    }

}
