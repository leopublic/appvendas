<?php

/**
 * @property int $id_pedido 
 * @property int $ClienteID 
 * @property int $UnidadeNegocioID 
 * @property int $CondicaoPagamentoID 
 * @property int $numero 
 * @property int $id_usuario_vendedor 
 * @property datetime $dt_pedido 
 * @property datetime $dt_sincronizacao 
 * @property datetime $dt_envio 
 * @property int $id_signus 
 * @property varchar $status_envio 
 * @property varchar $msg_envio 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 */
class Pedido extends Modelo {

    protected $table = 'pedido';
    protected $primaryKey = 'id_pedido';
    protected $guarded = array();

    public function scopePendentes() {
        return $this->where('status_envio', '<>', 1);
    }

    public function itens() {
        return $this->hasMany('ItemPedido', 'id_pedido');
    }

    public function cliente(){
        return $this->belongsTo('Cliente', 'ClienteID');
    }
    
    public function vendedor(){
        return $this->belongsTo('Usuario', 'id_usuario_vendedor');
    }
    
    public function getDtPedidoFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['dt_pedido']);
    }

    public function getDtSincronizacaoFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['dt_sincronizacao']);
    }

    public function getDtEnvioFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['dt_envio']);
    }
     
    public function getStatusEnvioDetalhadoAttribute($valor) {
        return $this->attributes['status_envio'];
    }
     
    public function getQtdItensAttribute($valor) {
        return 0;
    }

    public function scopeDoVendedor($query, $id_usuario_vendedor = ''){
        if($id_usuario_vendedor != ''){
            return $query->where('id_usuario_vendedor', '=', $id_usuario_vendedor);
        } else {
            return $query;
        }
    }

    public function scopeDoCliente($query, $ClienteID = ''){
        if($ClienteID != ''){
            return $query->where('ClienteID', '=', $ClienteID);
        } else {
            return $query;
        }
    }

    public function scopePedidoDesde($query, $dt_pedido = ''){
        if($dt_pedido != ''){
            return $query->where('dt_pedido', '>=', $dt_pedido);
        } else {
            return $query;
        }
    }

    public function scopePedidoAte($query, $dt_pedido = ''){
        if($dt_pedido != ''){
            return $query->where('dt_pedido', '<=', $dt_pedido);
        } else {
            return $query;
        }
    }
}
