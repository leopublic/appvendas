<?php
/**
 * @property int $ClienteID 
 * @property varchar $Codigo 
 * @property varchar $CNPJ 
 * @property varchar $RazaoSocial 
 * @property varchar $NomeFantasia 
 * @property varchar $Endereco 
 * @property varchar $Bairro 
 * @property varchar $CEP 
 * @property varchar $MunicipioID 
 * @property varchar $Municipio 
 * @property varchar $EstadoID 
 * @property varchar $EstadoSigla 
 * @property timestamp $created_at 
 * @property timestamp $updated_at  
 */
class Cliente extends Modelo {
    protected $table = 'cliente';
    protected $primaryKey = 'ClienteID';

    protected $guarded = array();
   
}