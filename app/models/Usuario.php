<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * @property int $id_usuario 
 * @property varchar $nome 
 * @property varchar $password 
 * @property varchar $email 
 * @property tinyint $fl_representante 
 * @property datetime $dt_ultimo_acesso 
 * @property datetime $dt_ultima_sincronizacao 
 * @property text $observacao 
 * @property timestamp $deleted_at 
 * @property timestamp $created_at 
 * @property timestamp $updated_at 
 * @property varchar $remember_token 
 * @property varchar $codigo 
 * @property int $signus_id 
 * @property varchar $razao_social 
 * @property varchar $fantasia 
 */

class Usuario extends Modelo implements UserInterface, RemindableInterface {

    protected $table = 'usuario';
    protected $hidden = array('password');
    protected $primaryKey = 'id_usuario';
    protected $guarded = array('dt_ultimo_acesso', 'id_perfil', 'created_at', 'updated_at');

    // =======================================
    // Controle de acesso
    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getReminderEmail() {
        return $this->email;
    }

    public function getRememberToken() {
        return $this->remember_token;
    }

    public function setRememberToken($value) {
        $this->remember_token = $value;
    }

    public function getRememberTokenName() {
        return 'remember_token';
    }

    // =========================================
    // Propriedades adicionais
    public function getDtUltimoAcessoFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['dt_ultimo_acesso']);
    }

    public function getCreatedAtFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['created_at']);
    }

    public function getDtUltimaSincronizacaoFmtAttribute($valor) {
        return $this->dtHrModelParaView($this->attributes['dt_ultima_sincronizacao']);
    }

    // ==========================================
    // Relacionamentos
    public function perfis() {
        return $this->belongsToMany('Perfil', 'usuario_perfil', 'id_usuario', 'id_perfil');
    }

    // ==========================================
    // Métodos
    public function atualizaUltimoAcesso() {
        $this->dt_ultimo_acesso = date('Y-m-d H:i:s');
        $this->save();
    }

    public function atualizaUltimaSincronizacao() {
        $this->dt_ultima_sincronizacao = date('Y-m-d H:i:s');
        $this->save();
    }

    public function zeraUltimaSincronizacao() {
        $this->dt_ultima_sincronizacao = null;
        $this->save();
    }

    public function arrayDePerfis() {
        return $this->perfis->lists('id_perfil');
    }

}
