<?php

/**
 * @property int $id_configuracao
 * @property varchar $codigo
 * @property varchar $valor
 * @property varchar $descricao
 * @property varchar $helper
 */
class Configuracao extends Modelo {

    protected $table = 'configuracao';
    protected $primaryKey = 'id_configuracao';
    protected $guarded = array();
    protected static $UnidadeNegocioConf;
    protected static $UrlSignus;
    protected static $UrlAppvendas;
    protected static $TamanhoCapa;
    protected static $TamanhoThumb;
    protected static $QualidadeJpeg;

    const CODIGO_UNIDADE_NEGOCIO = 'unidade_negocio';
    const CODIGO_URL_SIGNUS = 'url_signus';
    const CODIGO_URL_APPVENDAS = 'url_appvendas';
    const CODIGO_TAMANHO_CAPA = 'tamanho_capa';
    const CODIGO_TAMANHO_THUMB = 'tamanho_thumb';
    const CODIGO_QUALIDADE_JPEG = 'qualidade_jpeg';

    public static function UnidadeNegocioID() {
        if (!is_object(self::$UnidadeNegocioConf)) {
            self::$UnidadeNegocioConf = \Configuracao::where('codigo', '=', self::CODIGO_UNIDADE_NEGOCIO)->first();
        }
        return self::$UnidadeNegocioConf->valor;
    }

    public static function url_signus() {
        if (!is_object(self::$UrlSignus)) {
            self::$UrlSignus = \Configuracao::where('codigo', '=', self::CODIGO_URL_SIGNUS)->first();
            if (is_object(self::$UrlSignus)){
                return self::$UrlSignus->valor;
            } else {
                return '';
            }
        }
        return self::$UrlSignus->valor;
    }

    public static function url_appvendas() {
        if (!is_object(self::$UrlAppvendas)) {
            self::$UrlAppvendas = \Configuracao::where('codigo', '=', self::CODIGO_URL_APPVENDAS)->first();
            if (is_object(self::$UrlAppvendas)){
                return self::$UrlAppvendas->valor;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function tamanho_capa() {
        if (!is_object(self::$TamanhoCapa)) {
            self::$TamanhoCapa = \Configuracao::where('codigo', '=', self::CODIGO_TAMANHO_CAPA)->first();
            if (is_object(self::$TamanhoCapa)){
                return self::$TamanhoCapa->valor;
            } else {
                return '';
            }
        } else {
                return self::$TamanhoCapa->valor;
        }
    }


    public static function tamanho_thumb() {
        if (!is_object(self::$TamanhoThumb)) {
            self::$TamanhoThumb = \Configuracao::where('codigo', '=', self::CODIGO_TAMANHO_THUMB)->first();
            if (is_object(self::$TamanhoThumb)){
                return self::$TamanhoThumb->valor;
            } else {
                return '';
            }
        } else {
                return self::$TamanhoThumb->valor;
        }
    }

    public static function qualidade_jpeg() {
        if (!is_object(self::$QualidadeJpeg)) {
            self::$QualidadeJpeg = \Configuracao::where('codigo', '=', self::CODIGO_QUALIDADE_JPEG)->first();
            if (is_object(self::$QualidadeJpeg)){
                return self::$QualidadeJpeg->valor;
            } else {
                return '';
            }
        } else {
            return self::$QualidadeJpeg->valor;
        }
    }

    public static function atualizaUnidadeNegocioID($valor){
        self::atualizaValor(self::CODIGO_UNIDADE_NEGOCIO, $valor);
    }

    public static function atualizaUrlSignus($valor){
        self::atualizaValor(self::CODIGO_URL_SIGNUS, $valor);
    }

    public static function atualizaUrlAppvendas($valor){
        self::atualizaValor(self::CODIGO_URL_APPVENDAS, $valor);
    }

    public static function atualizaTamanhoCapa($valor){
        self::atualizaValor(self::CODIGO_TAMANHO_CAPA, $valor);
    }

    public static function atualizaTamanhoThumb($valor){
        self::atualizaValor(self::CODIGO_TAMANHO_THUMB, $valor);
    }

    public static function atualizaQualidadeJpeg($valor){
        self::atualizaValor(self::CODIGO_QUALIDADE_JPEG, $valor);
    }

    public static function atualizaValor($atributo, $valor){
        \Configuracao::where('codigo', '=', $atributo)->update(array('valor' => $valor));
        $conf = \Configuracao::where('codigo', '=', $atributo)->first();
        if (!is_object($conf)){
            $conf = new \Configuracao;
            $conf->codigo = $atributo;
        }
        $conf->valor = $valor;
        $conf->save();

    }
}