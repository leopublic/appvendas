<?php

/**
 * @property string $catalogo
 * @property string $titulo
 * @property string $artista 
 * @property float $preco 
 * @property string $generos
 * @property string $imagem_destaque 
 * @property string $imagem_listagem 
 * @property string $texto_cheio
 * @property string $texto_reduzido 
 * @property boolean $icms_isento 
 * @property boolean $coletanea 
 * @property boolean $mais_vendido 
 * @property boolean $infantil 
 * @property boolean $lancamento
 * @property dateTime $dt_importacao
 * @property text $texto_cheio
 * @property text $texto_reduzido
 * @property integer $id_status_produto
 * @property integer $id_classe
 * @property dateTime $dt_liberacao
 * @property integer $id_usuario_liberacao
 * @property dateTime $dt_bloqueio
 * @property integer $id_usuario_bloqueio
 * @property varchar $codigo_barras
 */
class Produto extends Modelo {

    protected $table = 'produto';
    protected $primaryKey = 'id_produto';

    const SUBDIR_IMAGENS = 'imagens/';
    const SUBDIR_VIDEO = 'videos/';
    const SUBDIR_AUDIO = 'audio/';
    const ARQ_CAPA = 'capa.jpg';
    const ARQ_THUMB = 'thumb.jpg';

    protected $guarded = array('id_genero');

    public function musicas() {
        return $this->hasMany('Musica', 'id_produto')->orderBy('ordem');
    }

    public function eventos() {
        return $this->hasMany('Evento', 'id_produto')->orderBy('dt_agendada', 'desc');
    }

    public function anexos() {
        return $this->hasMany('Anexo', 'id_produto');
    }

    public function generos() {
        return $this->belongsToMany('Genero', 'produto_genero', 'id_produto', 'id_genero');
    }

    public function classe() {
        return $this->belongsTo('Classe', 'id_classe');
    }

    public function arrayDeGeneros() {
        return $this->generos->lists('id_genero');
    }

    /**
     * Garante que os subdiretórios do produto estão ok e criados
     */
    public function diretoriosOk() {
        $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_IMAGENS;
        $this->diretorioOk($dir);
        $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_VIDEO;
        $this->diretorioOk($dir);
        $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_AUDIO;
        $this->diretorioOk($dir);
    }

    public function caminhoCapa() {
        $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_IMAGENS;
        if (!is_dir($dir)){
            mkdir($dir, 0775, true);
        }
        return $dir . self::ARQ_CAPA;
    }

    public function caminhoThumb() {
        $dir = public_path() . Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_IMAGENS ;
        if (!is_dir($dir)){
            mkdir($dir, 0775, true);
        }
        
        return $dir . self::ARQ_THUMB;
    }

    public function urlCapa() {
        if (File::exists($this->caminhoCapa())) {
            return URL::to(Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_IMAGENS . self::ARQ_CAPA);
        } else {
            return URL::to(Config::get('appvendas.caminho_media') . "/sem_capa.jpg");
        }
    }

    public function urlThumb() {
        if (File::exists($this->caminhoThumb())) {
            return URL::to(Config::get('appvendas.caminho_media') . "/" . $this->id_produto . "/" . self::SUBDIR_IMAGENS . self::ARQ_THUMB);
        } else {
            return URL::to(Config::get('appvendas.caminho_media') . "/sem_thumb.jpg");
        }
    }

    public function diretorioOk($dir) {
        if (!File::isDirectory($dir)) {
            \File::makeDirectory($dir, 755, true);
        }
    }

    public function getNovoAttribute() {
        if ($this->attributes['id_status_produto'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getDescricaoClasseAttribute() {
        if (is_object($this->classe)) {
            return $this->classe->descricao;
        } else {
            return '--';
        }
    }

    public function scopeLiberados($query) {
        return $query->where('id_status_produto', '=', 2);
    }

    public function liberar($id_usuario) {
        $this->id_status_produto = 2;
        $this->id_usuario_liberacao = $id_usuario;
        $this->dt_liberacao = date('Y-m-d H:i:s');
        $this->save();
    }

    public function bloquear($id_usuario) {
        $this->id_status_produto = 1;
        $this->id_usuario_bloqueio = $id_usuario;
        $this->dt_bloqueio = date('Y-m-d H:i:s');
        $this->save();
    }

    public function relacionados() {
        $sql = "select f.id_familia, p.prod_cod, p.id_produto, p.titulo, p.artista, p.catalogo, f.nome"
                . " from produto_familia pf "
                . " join produto p on p.id_produto = pf.id_produto"
                . " join familia f on f.id_familia = pf.id_familia"
                . " where pf.id_familia in (select id_familia from produto_familia where id_produto = ".$this->id_produto.") "
                . " ";
        $regs = \DB::select(\DB::raw($sql));
        return $regs;
    }
}