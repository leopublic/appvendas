<?php
/**
 * Arquivo de configuração do ambiente. 
 */
return array(
    /**
     * Raiz do caminho onde estão os arquivos de midia dos produtos para serem importados
     */
    'caminho_media_importacao_destaque' => '/var/www/capas_dest',
    'caminho_media_importacao_thumb' => '/var/www/capas_th',
    /**
     * Raiz do caminho onde esta o arquivo texto com os produtos a serem importados
     * - Use barras "/" para separar os diretórios
     * - Termine o diretório com "/"
     */
    'caminho_arquivo_importacao' => '/var/www/app-vendas/',
    /**
     * Raiz do caminho onde estão os arquivos de midia dos produtos no CMS
     * - Use barras "/" para separar os diretórios
     * - Não inicie com "/"
     * - Termine o diretório com "/"
     * 
     * O caminho informado aqui será um diretório criado dentro do subdiretório
     * /public da aplicação.
     * 
     * O CMS armazenará os arquivos de mídia numa estrutura conforme a seguir, 
     * a partir da raiz informada no parametro "caminho_media".
     * /media
     *    /(id do produto)
     *       /imagens
     *           /capa.jpg
     *           /thumb.jpg
     *       /videos
     *           /(id_video).(extensao)
     *       /audio
     *           /(id_audio).(extensao)
     */
    'caminho_media' => '/media',
);
