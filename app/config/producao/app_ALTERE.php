<?php
/**
 * Arquivo de configuração do ambiente. 
 * Faça as alterações desejadas e renomeie esse arquivo para app.php
 * 
 * ATENÇÃO: Esse arquivo está fora do controle de versão
 */
return array(
    /**
     * URL que será direcionada para a aplicação
     * Ex.: 'http://appvendas.somlivre.com.br'
     */
    'url' => 'http://appvendas.somlivre.com.br',
    /**
     * Modo de execução da aplicação. Usar "true" até que a aplicação esteja estável.
     * true  => Dá mensagens de erro detalhadas
     * false => Dá mensagens de erro resumidas
     */
    'debug' => true,

);
