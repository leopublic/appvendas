<?php
/**
 * Arquivo de configuração do ambiente. 
 * Faça as alterações desejadas e renomeie esse arquivo para appvendas.php
 * 
 * ATENÇÃO: Esse arquivo está fora do controle de versão
 */
return array(
    /**
     * Raiz do caminho onde estão os arquivos de midia dos produtos para serem importados
     */
    'caminho_media_importacao' => '/var/www/_media/',
    /**
     * Raiz do caminho onde esta o arquivo texto com os produtos a serem importados
     */
    'caminho_arquivo_importacao' => '/var/www/appvendas/',
    /**
     * Raiz do caminho onde estão os arquivos de midia dos produtos no CMS
     * 
     * O caminho informado aqui será um diretório criado dentro do subdiretório
     * /public da aplicação.
     * 
     * O CMS armazenará os arquivos de mídia numa estrutura conforme a seguir, 
     * a partir da raiz informada no parametro "caminho_media".
     * /media
     *    /(id do produto)
     *       /imagens
     *           /capa.jpg
     *           /thumb.jpg
     *       /videos
     *           /(id_video).(extensao)
     *       /audio
     *           /(id_audio).(extensao)
     */
    'caminho_media' => '/media',
);
