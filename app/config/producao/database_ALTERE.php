<?php
/**
 * Arquivo de configuração do banco de dados. 
 * Faça as alterações desejadas e renomeie esse arquivo para database.php
 * 
 * ATENÇÃO: Esse arquivo está fora do controle de versão
 */
return array(
    /**
     * Nome da conexão default a ser utilizada.
     * NÃO É NECESSÁRIO ALTERAR
     * 
     * A mudança de conexão sem revisão da aplicação pode gerar resultados 
     * inesperados por incompatibilidade dos comandos SQL entre os mecanismos
     * de SGBD.
     */    
    'default' => 'mysql',
    /**
     * Configuração das conexões disponíveis para a aplicação.
     * É OBRIGATÓRIO A REVISÃO DA CONEXÃO mysql
     */    
    'connections' => array(
            'mysql' => array(
                    'driver'    => 'mysql',
                    'host'      => '192.168.56.101',
                    'database'  => 'appvendas',
                    'username'  => 'root',
                    'password'  => 'mysql',
                    'charset'   => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix'    => '',
            ),
    ),
    /**
     * Nome da tabela com as alterações no banco de dados
     * NÃO É NECESSÁRIO ALTERAR
     */
    'migrations' => 'migrations',
);
