<?php

class UsuarioController extends BaseController {

    public function getIndex(){
        $usuarios = \Usuario::orderBy('nome')->get();
        return View::make('usuario.index')
                        ->with('registros', $usuarios)
        ;
    }

    public function getEdite($id_usuario = '0') {
        if ($id_usuario > 0){
            $model = \Usuario::find($id_usuario);            
        } else {
            $model = new \Usuario;
        }
        return View::make('usuario.edite')
                        ->with('model', $model)
            ;
    }

    public function postEdite() {
        try{
            Input::flash();
            $id_usuario = Input::get('id_usuario');
            $rep = new \Appvendas\Repositorios\RepositorioUsuario();
            $usuario = $rep->edita(Input::all());
            if($usuario){
                if (intval($id_usuario) == 0){
                    $msg = 'Usuário criado com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/usuario/alteresenha/'.$usuario->id_usuario);
                } else {
                    $msg = 'Usuário alterado com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/usuario');
                }
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }            
        } catch (Exception $ex) {
            Session::flash('flash_error', 'Não foi possível salvar: '.$ex->getMessage());
            return Redirect::back();
        }
    }

    public function getAlteresenha($id_usuario) {
        $model = \Usuario::find($id_usuario);            
        return View::make('usuario.alteresenha')
                        ->with('model', $model)
            ;
    }

    public function getEnvienovasenha($id_usuario) {
        try{
            $usuario = \Usuario::find($id_usuario);
            $rep = new \Appvendas\Repositorios\RepositorioUsuario();
            if($rep->enviaNovaSenha($usuario)){
                $msg = 'Nova senha enviada com sucesso';
                Session::flash('flash_msg', $msg);
                return Redirect::to('/usuario');
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
            return Redirect::back();
        }
    }

    public function postAlteresenha() {
        try{
            Input::flash();
            $rep = new \Appvendas\Repositorios\RepositorioUsuario();
            if($rep->altereSenha(Input::all())){
                $msg = 'Senha alterada com sucesso';
                Session::flash('flash_msg', $msg);
                return Redirect::to('/usuario');
            } else {
                return Redirect::to('/usuario/alteresenha/'.Input::get('id_usuario'))->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
            return Redirect::back();
        }
    }
    
    public function getExclua($id_usuario){
        $rep = new \Appvendas\Repositorios\RepositorioUsuario();
        $usuario = \Usuario::find($id_usuario);
        if($rep->exclua($usuario)){
            $msg = 'Usuário excluído com sucesso';
            Session::flash('flash_msg', $msg);
            return Redirect::to('/usuario');
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }
    
    public function getAltereminhasenha() {
        $usuario = Auth::user();
        return View::make('usuario.altereminhasenha')
                        ->with('model', $usuario)
        ;
    }
    public function postAltereminhasenha() {
        try{
            Input::flash();
            $rep = new \Appvendas\Repositorios\RepositorioUsuario();
            if($rep->altereMinhaSenha(Input::all())){
                $msg = 'Senha alterada com sucesso';
                Session::flash('flash_msg', $msg);
                return Redirect::to('/');
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', $ex->getMessage());
            return Redirect::back();
        }
    }
}
