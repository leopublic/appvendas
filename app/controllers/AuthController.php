<?php

/**
 * Controla a verificação de acessos
 */
class AuthController extends \BaseController {

    public function getLogin() {
        return View::make('usuario.login');
    }

    public function postLogin() {
        $username = Input::get('email');
        $password = Input::get('password');
        if (Auth::attempt(array('email' => $username, 'password' => $password), true)) {
            Auth::user()->atualizaUltimoAcesso();
            return Redirect::to('/');
        } else {
            return Redirect::to('auth/login')->with('login_errors', true)->with('msg', 'Usuário ou senha inválidos..');
        }
    }

    public function getEsquecisenha() {
        return View::make('usuario.esquecisenha');
    }

    public function postEsquecisenha() {
        if(Input::has('email')){
            $usuario = \Usuario::where('email', '=', Input::get('email'))->first();
            if (is_object($usuario)){
                $rep = new \Appvendas\Repositorios\RepositorioUsuario;
                $rep->enviaNovaSenha($usuario);
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nova senha enviada para o e-mail "'. Input::get('email') . '". Verifique na sua caixa de entrada.');
                return Redirect::to('/auth/login');
            } else {
                Session::flash('login_errors', true);
                Session::flash('msg', 'Nenhum usuário encontrado com o e-mail informado');
                return Redirect::back();
            }
        } else {
                Session::flash('login_errors', true);
            Session::flash('msg', 'Informe o e-mail');
            return Redirect::back();
        }
    }

    public function getLogout() {
        Session::clear();
        Auth::logout();
        return Redirect::to('/');
    }

}
