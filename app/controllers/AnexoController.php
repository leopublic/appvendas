<?php

class AnexoController extends BaseController {


    public function getPublicidade(){

        $anexos = \Anexo::where('id_produto', '=', 0)->get();
        return View::make('anexo.lista_publicidade')
                        ->with('anexos', $anexos)
        ;

    }

    public function postNovovideo($id_produto) {
        if (Input::hasFile('file') && Input::file('file')->isValid()) {
            $anexo = $this->capturarFile($_FILES["file"]);
            $anexo->id_produto = $id_produto;
            $anexo->id_tipo = 2;
            $anexo->save();
            $this->mover($_FILES["file"]['tmp_name'], $anexo->caminho());
            return "Arquivo criado com sucesso: ".$anexo->id_anexo;
        } else {
            return "Arquivo não recebido";
        }

    }

    public function postNovapublicidade() {
        if (Input::hasFile('file') && Input::file('file')->isValid()) {
            $anexo = $this->capturarFile($_FILES["file"]);
            $anexo->id_produto = 0;
            $anexo->id_tipo = 2;
            $anexo->save();
            $this->mover($_FILES["file"]['tmp_name'], $anexo->caminho());
            return "Arquivo criado com sucesso: ".$anexo->id_anexo;
        } else {
            return "Arquivo não recebido";
        }
    }

    public function postStorecapas() {
        try{
            $produto = Produto::find(Input::get('id_produto'));
            $tamCapa = \Configuracao::tamanho_capa();
            $tamThumb = \Configuracao::tamanho_thumb();
            $qualidade = \Configuracao::qualidade_jpeg();
            $rep = new \Appvendas\Repositorios\RepositorioAnexo;
            $msg = '';
            if (!empty($_FILES)) {
                if (isset($_FILES["capa"]) && $_FILES["capa"]['tmp_name'] != '' ){
                    $filename = $_FILES['capa']['name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (strtolower($ext) != 'jpg'){
                        throw new Exception('Extensão "'.$ext.'" não autorizada. Use somente arquivos JPG.');
                    }
                }
                if (isset($_FILES["capa_thumb"]) && $_FILES["capa_thumb"]['tmp_name'] != ''){
                    $filename = $_FILES['capa_thumb']['name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (strtolower($ext) != 'jpg'){
                        throw new Exception('Extensão "'.$ext.'" não autorizada. Use somente arquivos JPG.');
                    }
                }

                if (isset($_FILES["capa"]) && $_FILES["capa"]['tmp_name'] != ''){
                    $this->mover($_FILES["capa"]['tmp_name'], $produto->caminhoCapa());
                    $rep->ajustarImagem($produto->caminhoCapa(), $tamCapa, $qualidade);
                }
                if (isset($_FILES["capa_thumb"]) && $_FILES["capa_thumb"]['tmp_name'] != ''){
                    $this->mover($_FILES["capa_thumb"]['tmp_name'], $produto->caminhoThumb());
                    $rep->ajustarImagem($produto->caminhoThumb(), $tamThumb, $qualidade);
                }
                $msg = "Capas carregadas com sucesso";
            } else {
                throw new Exception("Arquivos de capa não informados");
            }
            Session::flash("flash_msg", $msg);
        }
        catch (Exception $e){
            Session::flash('flash_error', 'Não foi possível carregar as capas: '.$e->getMessage());
        }
        return Redirect::to('produto/capas/'.Input::get('id_produto'));
    }

    public function capturarFile($arquivo) {
        $path_parts = pathinfo($arquivo["name"]);
        $anexo = new \Anexo();
        $anexo->extensao = $path_parts['extension'];
        $anexo->nome_original = $arquivo["name"];
        return $anexo;
    }

    public function mover($origem, $destino) {
        if (file_exists($destino)) {
            unlink($destino);
        }
        move_uploaded_file($origem, $destino); //6
    }

    public function getDel($id_anexo){
        $anexo = Anexo::find($id_anexo);
        $id_produto = $anexo->id_produto;
        try {
            unlink($anexo->caminho());
        }
        catch (Exception $e){

        }
        $anexo->delete();
        if ($id_produto > 0){
            return Redirect::to('/produto/videos/'.$id_produto)->with('flash_msg', 'Anexo removido com sucesso');
        } else {
            return Redirect::to('/anexo/publicidade/')->with('flash_msg', 'Publicidade removida com sucesso');
        }
    }

    public function getVisualizar($id_anexo){
        $anexo = Anexo::find($id_anexo);
        return View::make('anexo.video')
                        ->with('anexo', $anexo)
        ;
    }

    public function getDoproduto($id_produto){
        $prd = Produto::find($id_produto);
        return View::make('anexo.index')
                        ->with('anexos', $prd->anexos )
        ;
    }

    public function getAcompanhaajuste(){
        return View::make('anexo.ajustar');
    }

    public function getAjustar(){
        $rep = new \Appvendas\Repositorios\RepositorioAnexo;
        $rep->AjustarTodasImagens();
    }

    public function getProgresso(){
        $rep = new \Appvendas\Repositorios\RepositorioAnexo;
        $ret = $rep->progresso();
        return json_encode($ret);
    }
}
