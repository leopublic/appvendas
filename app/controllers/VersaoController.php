<?php

class VersaoController extends BaseController {
    public function getShow($id_versao = 0){
        if ($id_versao > 0){
            $versao = \Versao::find($id_versao);
        } else {
            $versao = new \Versao;
        }
        $versoes = \Versao::orderBy('fl_atual', 'desc')->orderBy('num_major', 'desc')->orderBy('num_minor', 'desc')->orderBy('num_patch', 'desc')->get();
        return View::make('versao.show')
                        ->with('versao', $versao)
                        ->with('versoes', $versoes)
                ;
    }

    public function postStore($id_versao = 0) {
        if ($id_versao > 0){
            $versao = \Versao::find($id_versao);
            $versao->observacao = Input::get('observacao');
            $versao->num_major = intval(Input::get('num_major'));
            $versao->num_minor = intval(Input::get('num_minor'));
            $versao->num_patch = intval(Input::get('num_patch'));
            $versao->save();
            Session::flash('flash_msg', 'Versão atualizada com sucesso!');
            return Redirect::to('/versao/show');           
        } else {
            if (!empty($_FILES)) {
                \DB::table('versaoapp')->update(array('fl_atual'=> 0));
                $versao = new \Versao();
                $versao->observacao = Input::get('observacao');
                $versao->num_major = intval(Input::get('num_major'));
                $versao->num_minor = intval(Input::get('num_minor'));
                $versao->num_patch = intval(Input::get('num_patch'));
                $versao->dt_disponivel = date('Y-m-d H:i:s');
                $versao->fl_atual = 1;
                $versao->save();
                $this->mover($_FILES["versao"]['tmp_name'], $versao->caminho());
                Session::flash('flash_msg', 'Versão criada com sucesso!');
                return Redirect::to('/versao/show');
            } else {
                Session::flash('flash_error', 'Arquivo não recebido!');
                return Redirect::to('/versao/show');
            }
        }
    }

    public function mover($origem, $destino) {
        if (file_exists($destino)) {
            unlink($destino);
        }
        move_uploaded_file($origem, $destino); //6
    }

    public function getTornaratual($id_versao){
        $versao = Versao::find($id_versao);
        \DB::table('versaoapp')->update(array('fl_atual'=> 0));
        $versao->fl_atual = 1;
        $versao->save();
        Session::flash('flash_msg', 'Versão alterada com sucesso!');
        return Redirect::to('/versao/show');
        
    }
    
    public function getDownload($id_versao){
        $versao = Versao::find($id_versao);
        
        $headers = array(
             'Content-Type: application/vnd.android.package-archive',
           );
        
        return Response::download($versao->caminho(), $versao->nome, $headers);
    }

    public function getAtual(){
        $versao = Versao::where('fl_atual', '=', '1')->first();
        $headers = array("Content-Type" => 'application/vnd.android.package-archive');
        return Response::download($versao->caminho(), $versao->nome, $headers);
    }

    public function capturarFile($arquivo) {
        $path_parts = pathinfo($arquivo["name"]);
        $versao = new \Versao();
        return $versao;
    }

    public function getDel($id_versao){
        $versao = Versao::find($id_versao);
        try {
            unlink($versao->caminho());
        }
        catch (Exception $e){

        }
        $versao->delete();
        return Redirect::to('/versao/show/')->with('flash_msg', 'Versão removida com sucesso');
    }
}
