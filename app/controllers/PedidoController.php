<?php

class PedidoController extends BaseController {

    public function getIndex(){
        $rep = new \Appvendas\Repositorios\RepositorioPedido;
        $pedidos = $rep->pedidosFiltro();
        
        $clientes = \Cliente::orderBy('RazaoSocial')->lists('ClienteID', 'RazaoSocial');
        $vendedores = \Usuario::where('fl_representante', '=', 1)->orderBy('nome')->lists('id_usuario', 'nome');
        
        return View::make('pedido.index')
                        ->with('registros', $pedidos)
                        ->with('clientes', $clientes)
                        ->with('vendedores', $vendedores)
        ;        
    }
    
    public function getClientes() {
        $signus = new \Appvendas\Servicos\Signus();
        $signus->abreConexao();
        $ret = $signus->carregaClientes(6, 15);
    }

    public function getCondpag(){
        $signus = new \Appvendas\Servicos\Signus();
        $signus->abreConexao();
        $ret = $signus->carregaCondicaoPagamento(6, 15);
    }
    
    public function getCriateste(){
        $pedido = new \Pedido;
        $pedido->ClienteID = 64;
        $pedido->id_usuario_vendedor = 33;
        $pedido->dt_pedido = '2015-04-07';
        $pedido->UnidadeNegocioID = Configuracao::UnidadeNegocioID();
        $pedido->CondicaoPagamentoID = 1;
        $pedido->save();
        
        $item = new ItemPedido;
        $item->id_pedido = $pedido->id_pedido;
        $item->id_produto = 41645;
        $item->preco = 10.5;
        $item->perc_desconto = 0.1;
        $item->qtd = 10;
        $item->dt_inclusao = date("Y-m-d H:i:s");
        $item->save();

        $item = new ItemPedido;
        $item->id_pedido = $pedido->id_pedido;
        $item->id_produto = 41746;
        $item->preco = 15.5;
        $item->perc_desconto = 0.1;
        $item->qtd = 20;
        $item->dt_inclusao = date("Y-m-d H:i:s");
        $item->save();
    }

    public function getEnviateste($id_pedido){
        $signus = new \Appvendas\Servicos\Signus;
        $signus->abreConexao();
        $pedido = \Pedido::find($id_pedido);
        $ret = $signus->enviaUmPedido($pedido);
    }
    
    public function getTesteenvio(){
        
        return View::make('pedido.teste_signus')
        ;        
        
    }
}
