<?php

class FamiliaController extends BaseController {

    public function getIndex() {
        $registros = \Familia::orderBy('nome')->get();
        return View::make('familia.index')
                        ->with('registros', $registros)
        ;
    }

    public function getEdite($id = '0') {
        if ($id > 0) {
            $model = \Familia::find($id);
        } else {
            $model = new \Familia;
        }
        $produtos = \Produto::whereRaw('id_produto in (select id_produto from produto_familia where id_familia = ' . $id . ')')->get();
        return View::make('familia.edite')
                        ->with('model', $model)
                        ->with('relacionados', $produtos)
        ;
    }

    public function postEdite() {
        try {
            Input::flash();
            $id = Input::get('id_familia');
            $rep = new \Appvendas\Repositorios\RepositorioFamilia();
            $familia = $rep->edita(Input::all());
            if ($familia) {
                if (intval($id) == 0) {
                    $msg = 'Família criada com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/familia');
                } else {
                    $msg = 'Família alterada com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/familia');
                }
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }
        } catch (Exception $ex) {
            Session::flash('flash_error', 'Não foi possível salvar: ' . $ex->getMessage());
            return Redirect::back();
        }
    }

    public function getExclua($id_familia) {
        $rep = new \Appvendas\Repositorios\RepositorioFamilia();
        $familia = \Familia::find($id_familia);
        if ($rep->exclua($familia)) {
            $msg = 'Família excluído com sucesso';
            Session::flash('flash_msg', $msg);
            return Redirect::to('/familia');
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }

    public function getFormpesquisa() {
        return View::make('familia.pesquisaProduto')
        ;
    }

    public function getPesquisar($id_familia, $titulo) {
        $produtos = \Produto::where('titulo', 'like', '%' . $titulo . '%')->get();
        return View::make('familia.pesquisaProdutoResultado')
                        ->with('produtos', $produtos)
                        ->with('id_familia', $id_familia)
        ;
    }

    public function getAdicionar($id_familia, $id_produto) {
        $ret = array();
        $prodfam = \ProdutoFamilia::where('id_produto', '=', $id_produto)->where('id_familia', '=', $id_familia)->first();
        if (!is_object($prodfam) || $prodfam->id_produto == '') {
            $prodfam = new \ProdutoFamilia;
            $prodfam->id_produto = $id_produto;
            $prodfam->id_familia = $id_familia;
            $prodfam->save();
            $ret['codret'] = 1;
        } else {
            $ret['codret'] = 2;
            $ret['msg'] = 'erro';
        }
        return json_encode($ret);
    }

    public function getRemover($id_familia, $id_produto) {
        \ProdutoFamilia::where('id_produto', '=', $id_produto)->where('id_familia', '=', $id_familia)->delete();
        $msg = 'Produto retirado com sucesso';
        Session::flash('flash_msg', $msg);
        return Redirect::to('/familia/edite/' . $id_familia);
    }

}
