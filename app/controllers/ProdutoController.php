<?php
class ProdutoController extends BaseController
{
    public function getIndex() {
        $titulo = Input::old('titulo');
        $artista = Input::old('artista');
        $id_classe = Input::old('id_classe');
        $prod_cod = Input::old('prod_cod');
        $catalogo = Input::old('catalogo');
        $classes = array('' => '(todos)') + \Classe::orderBy('descricao')->lists('descricao', 'id_classe');
        $produtos = Produto::where('id_status_produto', '=', 2);
        if (trim($titulo) != '') {
            $produtos = $produtos->where('titulo', 'like', "%" . $titulo . "%");
        }
        if (trim($artista) != '') {
            $produtos = $produtos->where('artista', 'like', "%" . $artista . "%");
        }
        if (trim($id_classe) != '') {
            $produtos = $produtos->where('id_classe', '=', $id_classe);
        }
        if (trim($prod_cod) != '') {
            $produtos = $produtos->where('prod_cod', '=', $prod_cod);
        }
        if (trim($catalogo) != '') {
            $produtos = $produtos->where('catalogo', 'like', "%".$catalogo."%");
        }
        $produtos = $produtos->with('classe')->orderBy('titulo')->paginate(20);
        return View::make('produto.index')->with('registros', $produtos)->with('classes', $classes);
    }

    public function postIndex() {
        return Redirect::to('produto')->withInput();
    }

    public function getNovos() {
        $titulo = Input::old('titulo');
        $artista = Input::old('artista');
        $produtos = Produto::where('titulo', 'like', "%" . $titulo . "%")->where('artista', 'like', "%" . $artista . "%")->where('id_status_produto', '=', 1)->orderBy('titulo')->paginate(20);
        return View::make('produto.novos')->with('registros', $produtos);
    }

    public function postNovos() {
        return Redirect::to('produto/novos')->withInput();
    }

    public function getCadastro($id_produto) {
        $produto = \Produto::find($id_produto);
        $classes = array('' => '(não informada)') + \Classe::orderBy('descricao')->lists('descricao', 'id_classe');
        return View::make('produto.cadastro')->with('nome_aba', 'Cadastro')->with('aba_ativa', 'cadastro')->with('classes', $classes)->with('obj', $produto);
    }

    public function postCadastro() {
        $id_produto = Input::get('id_produto');
        $produto = Produto::find($id_produto);
        $produto->fill(Input::all());
        $produto->save();
        $generos = Input::get('id_genero');
        if (!is_array($generos) || count($generos) == 0) {
            $generos = array();
        }
        $produto->generos()->sync($generos);
        Session::flash('flash_msg', 'Produto atualizado com sucesso!');
        return Redirect::to('/produto/cadastro/' . $id_produto);
    }

    public function getMusicas($id_produto) {
        $produto = Produto::find($id_produto);

        return View::make('produto.musicas')->with('nome_aba', 'Músicas')->with('aba_ativa', 'musicas')->with('icon', 'icon-volume-up')->with('obj', $produto);
    }

    public function getVideos($id_produto) {
        $produto = Produto::find($id_produto);

        return View::make('produto.videos')->with('nome_aba', 'Vídeos')->with('aba_ativa', 'videos')->with('icon', 'icon-film')->with('obj', $produto);
    }

    public function getEventos($id_produto, $id_evento = 0) {
        $produto = Produto::find($id_produto);
        if ($id_evento > 0) {
            $evento = Evento::find($id_evento);
        }
        else {
            $evento = new Evento;
        }

        return View::make('produto.eventos')
            ->with('nome_aba', 'Eventos')
            ->with('aba_ativa', 'eventos')
            ->with('icon', 'icon-calendar')
            ->with('obj', $produto)
            ->with('evento', $evento);
    }

    /**
     * Retorna a tela de capas.
     * @param  int $id_produto Chave do produto
     * @return string             view
     */
    public function getCapas($id_produto) {
        $produto = Produto::find($id_produto);

        return View::make('produto.capas')->with('nome_aba', 'Capas')->with('aba_ativa', 'capas')->with('icon', 'icon-picture')->with('obj', $produto);
    }

    public function getShow($id_produto) {
        $produto = Produto::find($id_produto);

        return View::make('produto.show')->with('obj', $produto);
    }

    public function getLiberar($id_produto) {
        $produto = Produto::find($id_produto);
        $produto->liberar(\Auth::user()->id_usuario);
        Session::flash('flash_msg', 'Produto liberado com sucesso!');
        return Redirect::to('/produto/cadastro/' . $id_produto);
    }

    public function getBloquear($id_produto) {
        $produto = Produto::find($id_produto);
        $produto->bloquear(\Auth::user()->id_usuario);
        Session::flash('flash_msg', 'Produto bloqueado com sucesso!');
        return Redirect::to('/produto/cadastro/' . $id_produto);
    }

    public function getFamilia($id_produto) {
        $produto = \Produto::find($id_produto);
        $relacionados = $produto->relacionados();
        return View::make('produto.familia')->with('nome_aba', 'Família')->with('aba_ativa', 'familia')->with('relacionados', $relacionados)->with('obj', $produto);
    }

    public function getCarregaxml() {

        return View::make('produto.carga');
    }

    public function postCarregaxml() {
        try {
            if ($_FILES["arquivoxml"]["error"] == 0) {
                if ($_FILES["arquivoxml"]["size"] > 0) {
                    $caminho = public_path() . DIRECTORY_SEPARATOR . 'importacao';

                    if (!is_dir($caminho)) {
                        mkdir($caminho, 0775, true);
                    }
                    $caminho.= DIRECTORY_SEPARATOR . "importacao.xml";
                    if (file_exists($caminho)) {
                        unlink($caminho);
                    }
                    move_uploaded_file($_FILES["arquivoxml"]['tmp_name'], $caminho);
                     //6
                    $xml = simplexml_load_file($caminho);
                    $lista = $xml->listaProdutos;
                    $qtd = 0;
                    $novos = 0;
                    $atualizados = 0;
                    foreach ($lista->produto as $produtoXML) {
                        $produto = \Produto::where('prod_cod', '=', trim($produtoXML->codigo))->first();
                        if ($produtoXML->codigo != '') {
                            if (!is_object($produto)) {
                                $produto = new \Produto();
                                $produto->prod_cod = $produtoXML->codigo;
                                $produto->dt_importacao = date('Y-m-d H:i:s');
                                $produto->id_status_produto = 1;
                                $novos++;
                            }
                            else {
                                $atualizados++;
                            }
                            $produto->catalogo = $produtoXML->catalogo;
                            $produto->titulo = $produtoXML->titulo;
                            $produto->artista = $produtoXML->artista;
                            $produto->icms_isento = $produtoXML->icms_isento;
                            $produto->mais_vendido = $produtoXML->mais_vendido;
                            $produto->coletanea = $produtoXML->coletanea;
                            $produto->infantil = $produtoXML->infantil;
                            $produto->lancamento = $produtoXML->lancamento;
                            $produto->codigo_barras = $produtoXML->codigo_barras;
                            $produto->preco = $produtoXML->preco;
                            $produto->save();
                            $qtd++;
                        }
                    }
                    Session::flash('flash_msg', 'Arquivo importado com sucesso!<br/>' . number_format($novos, 0, ",", ".") . " produtos adicionados e " . number_format($atualizados, 0, ",", ".") . " produtos alterados.");
                    return Redirect::to('/produto/carregaxml/');
                }
                else {
                    Session::flash('flash_error', 'Arquivo não informado');
                    return Redirect::to('/produto/carregaxml/');
                }
            }
            else {
                Session::flash('flash_error', 'Erro no recebimento do arquivo:' . $_FILES["arquivoxml"]["error"]);
                return Redirect::to('/produto/carregaxml/');
            }
        }
        catch(Exception $ex) {
            Session::flash('flash_error', 'Ocorreu um problema:' . $ex->getMessage());
            return Redirect::to('/produto/carregaxml/');
        }
    }
}
