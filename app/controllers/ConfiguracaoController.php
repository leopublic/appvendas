<?php
class ConfiguracaoController extends BaseController {
    public function getShow(){
        return View::make('configuracao.show')
                ->with('UnidadeNegocioID', \Configuracao::UnidadeNegocioID())
                ->with('url_signus', \Configuracao::url_signus())
                ->with('url_appvendas', \Configuracao::url_appvendas())
                ->with('tamanho_capa', \Configuracao::tamanho_capa())
                ->with('tamanho_thumb', \Configuracao::tamanho_thumb())
                ->with('qualidade_jpeg', \Configuracao::qualidade_jpeg())
                ;

    }

    public function postStore(){
        \Configuracao::atualizaUnidadeNegocioID(Input::get('UnidadeNegocioID'));
        \Configuracao::atualizaUrlAppvendas(Input::get('url_appvendas'));
        \Configuracao::atualizaUrlSignus(Input::get('url_signus'));
        \Configuracao::atualizaTamanhoCapa(Input::get('tamanho_capa'));
        \Configuracao::atualizaTamanhoThumb(Input::get('tamanho_thumb'));
        \Configuracao::atualizaQualidadeJpeg(Input::get('qualidade_jpeg'));
        Session::flash('flash_msg', 'Configurações atualizadas com sucesso');
        return Redirect::to('/configuracao/show');
    }
}