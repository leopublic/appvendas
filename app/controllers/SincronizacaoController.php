<?php
class SincronizacaoController extends BaseController {
    public function getLogin($email, $password){
        
    }
    public function getProdutos($login = '', $senha = ''){
        // Incluir processo de validação
        $produtos = \Produto::liberados()->toJson();
//        $ret = json_encode($produtos);
        $response = Response::make($produtos, 200);
        $response->header('charset', 'utf-8');
        return $response;
    }
    
    public function getMusicas(){
        $musicas = \Musica::all()->toJson();
        $response = Response::make($musicas, 200);
        $response->header('charset', 'utf-8');
        return $response;
    }
    
    public function getEventos(){
        $eventos = \Evento::all()->toJson();
        $response = Response::make($eventos, 200);
        $response->header('charset', 'utf-8');
        return $response;
    }
    
    public function getIndex(){
        $email = Input::get('email');
        $password = Input::get('password');
        if (Auth::attempt(array('email' => $email, 'password' => $password), true)) {
            Auth::user()->atualizaUltimaSincronizacao();
            return $this->geraSincronizacao(Auth::user()->id_usuario);
        } else {
            return "Credencial inválida";
        }
        
    }

    public function geraSincronizacao($id_usuario = ''){
        $produtos = \Produto::liberados()->orderBy('id_produto')->get()->toArray();
        foreach($produtos as &$produto){
            $produto['texto_cheio'] = nl2br($produto['texto_cheio']);
            $produto['texto_reduzido'] = nl2br($produto['texto_reduzido']);
        }
        $musicas = \Musica::all()->toArray();
        $eventos = \Evento::all()->toArray();
        $classes = \Classe::all()->toArray();
        foreach($eventos as &$evento){
            $evento['descricao'] = nl2br($evento['descricao']);
        }

        $generos = \Genero::all()->toArray();
        
        $anexos = \Anexo::all()->toArray();
        
        $produtos_familias = \ProdutoFamilia::all()->toArray();

        $familias = \Familia::all()->toArray();

        $sql = "select id_produto, id_genero from produto_genero";
        $produto_genero = \ProdutoGenero::all()->toArray();
        
        if ($id_usuario > 0){
//            $signus = new \Appvendas\Servicos\Signus;
//            $signus->abreConexao();
////            $clientes = $signus->clientesDoVendedor($id_usuario);
//            $condicoespagamento = $signus->condicoesDoVendedor($id_usuario);
        } else {
//            $clientes = array();
//            $condicoespagamento = array();
        }
        
        // Monta configuracoes
        $rep = new \Appvendas\Repositorios\RepositorioConfiguracao();
        $config = array(
            'UnidadeNegocioID' => \Configuracao::UnidadeNegocioID()
            ,'url_signus' => \Configuracao::url_signus()
            ,'url_appvendas' => \Configuracao::url_appvendas()
            ,'versaoAtual' => $rep->versaoMaisRecente()
        );
        
        $ret = array(
                    'produtos' => $produtos
                ,   'thumbs' => $produtos
                ,   'musicas'=>$musicas
                ,   'eventos'=>$eventos
                ,   'anexos'=>$anexos
                ,   'generos'=>$generos
                ,   'classe'=>$classes
                ,   'familias'=>$familias
                ,   'produtos_familias'=>$produtos_familias
//                ,   'clientes'=>$clientes
//                ,   'condicoespagamento'=>$condicoespagamento
                ,   'produto_genero'=>$produto_genero
                , 'configuracao' => $config
            );
        
        $response = Response::make(json_encode($ret), 200);
        $response->header('charset', 'utf-8');
        return $response;        
    }
    
    public function getImagens(){
        // Incluir processo de validação
        $produtos = \Imagem::all()->toJson();
        return $produtos;
    }
    public function getVideos(){
        // Incluir processo de validação
        $produtos = \Anexo::all()->toJson();
        return $produtos;
    }
    public function postPedidos(){
        $pedidos = Input::get("pedidos");
        
        
    }
}