<?php

class EventoController extends BaseController {
    public function postStore(){
        if (Input::get('id_evento') > 0){
            $evento = Evento::find(Input::get('id_evento'));
        } else {
            $evento = new Evento();            
        }
        $evento->fill(Input::all());
        $evento->save();
        return Redirect::to('produto/eventos/'.Input::get('id_produto'))->with('flash_msg', 'Evento criado com sucesso');
    }
    public function getDel($id_evento){
        $evento = Evento::find($id_evento);
        $id_produto = $evento->id_produto;
        Evento::destroy($id_evento);
        return Redirect::to('produto/eventos/'.$id_produto)->with('flash_msg', 'Evento excluído com sucesso');
    }
}
