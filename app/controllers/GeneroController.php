<?php

class GeneroController extends BaseController {

    public function getIndex(){
        $registros = \Genero::orderBy('nome')->get();
        return View::make('genero.index')
                        ->with('registros', $registros)
        ;
    }

    public function getEdite($id = '0') {
        if ($id > 0){
            $model = \Genero::find($id);            
        } else {
            $model = new \Genero;
        }
        return View::make('genero.edite')
                        ->with('model', $model)
            ;
    }

    public function postEdite() {
        try{
            Input::flash();
            $id = Input::get('id_genero');
            $rep = new \Appvendas\Repositorios\RepositorioGenero();
            $genero = $rep->edita(Input::all());
            if($genero){
                if (intval($id) == 0){
                    $msg = 'Gênero criado com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/genero');
                } else {
                    $msg = 'Gênero alterado com sucesso';
                    Session::flash('flash_msg', $msg);
                    return Redirect::to('/genero');
                }
            } else {
                return Redirect::back()->withErrors($rep->getErrors());
            }            
        } catch (Exception $ex) {
            Session::flash('flash_error', 'Não foi possível salvar: '.$ex->getMessage());
            return Redirect::back();
        }
    }

    public function getExclua($id_genero){
        $rep = new \Appvendas\Repositorios\RepositorioGenero();
        $genero = \Genero::find($id_genero);
        if($rep->exclua($genero)){
            $msg = 'Gênero excluído com sucesso';
            Session::flash('flash_msg', $msg);
            return Redirect::to('/genero');
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }
}
