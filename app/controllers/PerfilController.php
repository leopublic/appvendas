<?php

class PerfilController extends BaseController {

    public function getIndex(){
        $perfis = \Perfil::orderBy('descricao')->get();
        return View::make('perfil.index')
                        ->with('registros', $perfis)
        ;
    }


    public function getEdite($id_perfil = '0') {
        if ($id_perfil > 0){
            $model = \Perfil::find($id_perfil);            
        } else {
            $model = new \Perfil;
        }
        return View::make('perfil.edite')
                        ->with('model', $model)
            ;
    }

    public function postEdite() {
        Input::flash();
        $id_perfil = Input::get('id_perfil');
        $rep = new \Appvendas\Repositorios\RepositorioPerfil();
        if($rep->edita(Input::all())){
            if (intval($id_perfil) == 0){
                $msg = 'Perfil criado com sucesso';
            } else {
                $msg = 'Perfil alterado com sucesso';
            }
            Session::flash('flash_msg', $msg);
            return Redirect::to('/perfil');
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }
    
    public function getExclua($id_perfil){
        $rep = new \Appvendas\Repositorios\RepositorioPerfil();
        $perfil = \Perfil::find($id_perfil);
        if($rep->exclua($perfil)){
            $msg = 'Perfil excluído com sucesso';
            Session::flash('flash_msg', $msg);
            return Redirect::to('/perfil');
        } else {
            return Redirect::back()->withErrors($rep->getErrors());
        }
    }
}
