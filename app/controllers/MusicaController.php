<?php

class MusicaController extends BaseController {

    public function postStore() {
        $musica = new Musica();
        $musica->id_produto = Input::get('id_produto');
        $musica->ordem = Input::get('ordem');
        $musica->nome = Input::get('nome');
        $musica->duracao = Input::get('duracao');
        $musica->save();
        $msg = "Música criada com sucesso.";
        if (!empty($_FILES)) {
            $path_parts = pathinfo($_FILES["trecho"]["name"]);
            $musica->nome_original = $path_parts['basename'];
            $musica->extensao = $path_parts['extension'];
            $musica->save();
            // Move o arquivo
            if (file_exists($musica->caminho())) {
                unlink($musica->caminho());
            }
            move_uploaded_file($_FILES["trecho"]['tmp_name'], $musica->caminho()); //6
            
            $msg.=" Trecho armazenado com sucesso.";
        }
        Session::flash('flash_msg', $msg);
        return Redirect::to('produto/musicas/' . Input::get('id_produto'));
    }

    public function getExclua($id_musica){
        $musica  = Musica::find($id_musica);
        $id_produto = $musica->id_produto;
        if (File::exists($musica->caminho())){
            File::delete($musica->caminho());
        }
        $musica->delete();
        Session::flash('flash_msg', 'Música excluída com sucesso.');
        return Redirect::to('produto/musicas/' . $id_produto);        
    }

    public function postTrecho() {
        $musica = Musica::find(Input::get('id_musica'));
        if (!empty($_FILES)) {
            $path_parts = pathinfo($_FILES["trecho"]["name"]);
            $musica->nome_original = $path_parts['basename'];
            $musica->extensao = $path_parts['extension'];
            $musica->save();
            // Move o arquivo
            log::info('Caminho destino:'.$musica->caminho());
            if (file_exists($musica->caminho())) {
                unlink($musica->caminho());
            }
            move_uploaded_file($_FILES["trecho"]['tmp_name'], $musica->caminho()); //6
            
            $msg =" Trecho armazenado com sucesso.";
        } else {
            $msg = "Arquivo não informado";
        }
        Session::flash('flash_msg', $msg);
        return Redirect::to('produto/musicas/' . $musica->id_produto);
    }
    
    
}
