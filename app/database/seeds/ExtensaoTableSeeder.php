<?php

class ExtensaoTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('extensao')->truncate();

        $extensao = array('extensao' => 'bmp'   , 'mime' => 'image/x-windows-bmp', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
		$extensao = array('extensao' => 'jpg'   , 'mime' => 'image/jpeg', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'png'   , 'mime' => 'image/png', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'gif'   , 'mime' => 'image/gif', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'tiff'  , 'mime' => 'image/tiff', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'pdf'   , 'mime' => 'application/pdf', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'doc'   , 'mime' => 'application/msword', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'docx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'xls'   , 'mime' => 'application/vnd.ms-excel', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'xlsx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'ppt'   , 'mime' => 'application/vnd.ms-powerpoint', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'pptx'  , 'mime' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'fl_video' => 0);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'avi'  , 'mime' => 'video/avi', 'fl_video' => 1);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'mov'  , 'mime' => 'video/quicktime', 'fl_video' => 1);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'mpeg'  , 'mime' => 'video/mpeg', 'fl_video' => 1);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'mpg'  , 'mime' => 'video/mpeg', 'fl_video' => 1);
        DB::table('extensao')->insert($extensao);
        $extensao = array('extensao' => 'mp4'  , 'mime' => 'video/mp4', 'fl_video' => 1);
        DB::table('extensao')->insert($extensao);

		// Uncomment the below to run the seeder
	}

}
