<?php

class GeneroTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('genero')->truncate();

        $obj = array("nome" => "Sertanejo");
        DB::table('genero')->insert($obj);
        $obj = array("nome" => "Gospel");
        DB::table('genero')->insert($obj);
        $obj = array("nome" => "Rock");
        DB::table('genero')->insert($obj);
        $obj = array("nome" => "MPB");
        DB::table('genero')->insert($obj);
    }

}