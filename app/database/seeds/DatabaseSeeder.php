<?php

class DatabaseSeeder extends Seeder {

    public function run() {
        Eloquent::unguard();
        $this->call('PerfilTableSeeder');
//        $this->call('GeneroTableSeeder');
//        $this->call('ProdutoTableSeeder');
//        $this->call('MusicaTableSeeder');
        $this->call('ExtensaoTableSeeder');
    }

}
