<?php

class MusicaTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('musica')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $obj = array("id_produto" => 1, "nome" => "Livre", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "3:30", "ordem" => 1);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Deixa Falar", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "7:80", "ordem" => 2);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Faces da Lua", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "8:90", "ordem" => 3);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Gaveta", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "1:20", "ordem" => 4);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Homens & Anjos", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "2:30", "ordem" => 5);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Mô", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "3:40", "ordem" => 6);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 1, "nome" => "Cruel", "autor" => "Fernando e Sorocaba", "artista" => "Fernando e Sorocaba", "duracao" => "4:50", "ordem" => 7);
        DB::table('musica')->insert($obj);

        $obj = array("id_produto" => 2, "nome" => "Amiga da Minha Irmã", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "1:20", "ordem" => 1);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "Até de Manhã", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "2:30", "ordem" => 2);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "Levemente Alterado", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "3:40", "ordem" => 3);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "Pegada", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "5:60", "ordem" => 4);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "Maria", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "6:70", "ordem" => 5);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "É Nóis Faze Parapapá", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "7:80", "ordem" => 6);
        DB::table('musica')->insert($obj);
        $obj = array("id_produto" => 2, "nome" => "Love Com Você", "autor" => "Michel Teló", "artista" => "Michel Teló", "duracao" => "8:90", "ordem" => 7);
        DB::table('musica')->insert($obj);


    }

}