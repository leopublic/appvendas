<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoTable extends Migration {

    public function up() {
        Schema::create('evento', function($table) {
            $table->increments('id_evento');
            $table->integer('id_produto')->unsigned();
            $table->date('data')->nullable();
            $table->string('titulo', 500)->nullable();
            $table->text('descricao')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        if (Schema::hasTable('evento')) {
            Schema::drop('evento');
        }
    }

}
