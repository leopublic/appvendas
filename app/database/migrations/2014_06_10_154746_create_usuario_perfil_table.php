<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsuarioPerfilTable extends Migration {

    public function up() {
        Schema::create('usuario_perfil', function($table) {
            $table->increments('id_usuario_perfil');
            $table->integer('id_usuario')->unsigned()->nullable();
            $table->integer('id_perfil')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('usuario_perfil');
    }

}
