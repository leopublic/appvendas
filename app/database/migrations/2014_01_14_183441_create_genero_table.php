<?php

use Illuminate\Database\Migrations\Migration;

class CreateGeneroTable extends Migration {

    public function up() {
        Schema::create('genero', function($table) {
            $table->increments('id_genero');
            $table->string('nome', 500)->nullable();
            $table->timestamps();
        });

        Schema::create('produto_genero', function($table) {
            $table->integer('id_genero');
            $table->integer('id_produto');
            $table->string('nome', 500)->nullable();
            $table->timestamps();
        });

        $now = date('Y-m-d H:i:s');
        $obj = array("id_genero" => "28", "nome" => "VÁRIOS", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "29", "nome" => "CLASSICOS / RELIGIOSOS / NEW AGE", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "30", "nome" => "MPB", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "31", "nome" => "TRADICIONAIS", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "32", "nome" => "ROQUEIRO", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "33", "nome" => "DVD / VHS", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "34", "nome" => "INFANTIL", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "35", "nome" => "ROMANTICOS / NOVELAS", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "36", "nome" => "AXÉ", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "37", "nome" => "GOSPEL", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "38", "nome" => "SERTANEJO", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "39", "nome" => "SAMBA / PAGODE", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "40", "nome" => "FUNK", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "41", "nome" => "FORRÓ", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "42", "nome" => "INSTRUMENTAL", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "43", "nome" => "POP", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "44", "nome" => "ROMÂNTICO", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "45", "nome" => "WORLD MUSIC", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "46", "nome" => "DANCE", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "47", "nome" => "COUNTRY", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "48", "nome" => "RELIGIOSO", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "49", "nome" => "GAFIEIRA", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "50", "nome" => "BREGA", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "51", "nome" => "POP ROCK", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "52", "nome" => "SERTANEJO UNIVERSITÁRIO", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "53", "nome" => "REGGAE", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "54", "nome" => "SAMBA ROCK", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "55", "nome" => "PAGODE", "created_at" => $now);
        DB::table('genero')->insert($obj);
        $obj = array("id_genero" => "56", "nome" => "ROCK", "created_at" => $now);
        DB::table('genero')->insert($obj);
        
    }

    public function down() {
        if (Schema::hasTable('genero')){
            Schema::drop('genero');
        }
        if (Schema::hasTable('produto_genero')){
            Schema::drop('produto_genero');
        }
    }

}
