<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableFamilia extends Migration {

    public function up() {
        Schema::create('familia', function($table) {
            $table->increments('id_familia');
            $table->string('nome', 500)->nullable();
            $table->timestamps();
        });
        
        Schema::create('produto_familia', function($table) {
            $table->integer('id_familia');
            $table->integer('id_produto');
            $table->timestamps();
        });
        
    }

    public function down() {
        Schema::dropIfExists('familia');
        Schema::dropIfExists('produto_familia');
    }

}
