<?php
/**
 * @property int $id_configuracao 
 * @property varchar $codigo 
 * @property varchar $valor 
 * @property varchar $descricao 
 * @property varchar $helper 
 */
use Illuminate\Database\Migrations\Migration;

class CreateTableConfiguracao extends Migration {

    public function up() {
        Schema::create('configuracao', function($table) {
            $table->increments('id_configuracao');
            $table->string('codigo', 50)->nullable();
            $table->string('valor')->nullable();
            $table->string('descricao', 200)->nullable();
            $table->string('helper', 200)->nullable();
            $table->timestamps();
        });
        
        $obj = array("codigo" => 'unidade_negocio', "valor" => '16', "descricao" => 'ID da Unidade de negócios que deve ser passada para o SIGNUS', "helper" => 'Preencher com o UNID_COD da tabela TGEN_UNIDADE');
        DB::table('configuracao')->insert($obj);

        $obj = array("codigo" => 'url_signus', "valor" => 'http://signusweb.somlivre.com.br:83/WSIntegracaoCMS.svc?wsdl', "descricao" => 'Url do webservice do SIGNUS');
        DB::table('configuracao')->insert($obj);

        $obj = array("codigo" => 'url_appvendas', "valor" => 'http://appvendas.somlivre.com.br', "descricao" => 'Url do serviço de sincronização de catálogo');
        DB::table('configuracao')->insert($obj);
    }

    public function down() {
        Schema::dropIfExists('configuracao');
    }

}
