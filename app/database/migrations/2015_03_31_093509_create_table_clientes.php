<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableClientes extends Migration {

    public function up() {
        Schema::create('cliente', function($table) {
            $table->integer('ClienteID');
            $table->integer('id_usuario')->unsigned();
            $table->string('Codigo')->nullable();
            $table->string('CNPJ')->nullable();
            $table->string('RazaoSocial')->nullable();
            $table->string('NomeFantasia')->nullable();
            $table->string('Endereco')->nullable();
            $table->string('Bairro')->nullable();
            $table->string('CEP')->nullable();
            $table->string('MunicipioID')->nullable();
            $table->string('Municipio')->nullable();
            $table->string('EstadoID')->nullable();
            $table->string('EstadoSigla')->nullable();
            $table->timestamps();
        });
        
        Schema::create('vendedor_cliente', function($table) {
            $table->increments('id_vendedor_cliente');
            $table->integer('ClienteID');
            $table->string('VendedorID')->nullable();
            $table->timestamps();
        });
        
    }

    public function down() {
        Schema::dropIfExists('cliente');
        Schema::dropIfExists('vendedor_cliente');
    }
}