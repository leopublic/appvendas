<?php

use Illuminate\Database\Migrations\Migration;

class AddExtensaoToMusica extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('musica', function($table) {
            $table->string('extensao', 20)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('musica', function($table) {
            $table->dropColumn('extensao');
        });
    }

}
