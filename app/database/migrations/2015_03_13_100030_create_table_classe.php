<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableClasse extends Migration {
    public function up() {
        Schema::create('classe', function($table) {
            $table->increments('id_classe');
            $table->string('descricao', 500)->nullable();
            $table->timestamps();
        });
        
        $now = date('Y-m-d H:i:s');
        $obj = array("descricao" => "Lançamento", "created_at" => $now);
        DB::table('classe')->insert($obj);
        $obj = array("descricao" => "Mídia", "created_at" => $now);
        DB::table('classe')->insert($obj);
        $obj = array("descricao" => "Destaque", "created_at" => $now);
        DB::table('classe')->insert($obj);
        $obj = array("descricao" => "Catálogo", "created_at" => $now);
        DB::table('classe')->insert($obj);
    }

    public function down() {
        if (Schema::hasTable('classe')) {
            Schema::drop('classe');
        }
    }

}