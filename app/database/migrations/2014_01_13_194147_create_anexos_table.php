<?php

use Illuminate\Database\Migrations\Migration;

class CreateAnexosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('anexo', function($table) {
            $table->increments('id_anexo');
            $table->integer('id_produto')->unsigned();
            $table->integer('id_tipo')->unsigned();
            $table->string('extensao', 20)->nullable();
            $table->string('nome', 1500)->nullable();
            $table->string('nome_original', 1500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('anexo');
    }

}
