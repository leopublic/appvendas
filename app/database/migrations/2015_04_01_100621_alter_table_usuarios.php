<?php

use Illuminate\Database\Migrations\Migration;

class AlterTableUsuarios extends Migration {

    public function up() {
        Schema::table('usuario', function($table) {
            $table->string('codigo')->nullable();
            $table->integer('signus_id')->unsigned()->nullable();
            $table->string('razao_social', 200)->nullable();
            $table->string('fantasia', 200)->nullable();
        });
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 6, "codigo" => 'AA23467', "razao_social" => 'GATO PINGADO REPR. LTDA.', "fantasia" => 'GIBA', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s") );
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 26, "codigo" => 'AC23465', "razao_social" => 'MUSICA LIVRE REPRES. LTDA', "fantasia" => 'VICTOR GOUVEA', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 42, "codigo" => 'BE23477', "razao_social" => 'TITANIS REPRES. LTDA', "fantasia" => 'TITANIS REPRES.', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 47, "codigo" => 'BF23470', "razao_social" => 'TITANIS REPRES. LTDA', "fantasia" => 'TITANIS REPRES.', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 121, "codigo" => 'DE23405', "razao_social" => 'SOM LIVRE - VENDAS LASA', "fantasia" => 'SOM LIVRE', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 199, "codigo" => 'EQ23405', "razao_social" => 'MARCELO MONTEIRO ROCHA', "fantasia" => 'MARCELO MONTEIRO', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 207, "codigo" => 'EQ23437', "razao_social" => 'SOM LIVRE - DEPARTAMENTO SP', "fantasia" => 'SOM LIVRE', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 296, "codigo" => 'ZG23001', "razao_social" => 'AMOSTRA GRATIS', "fantasia" => 'AMOSTRA GRATIS', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 302, "codigo" => 'ZD23001', "razao_social" => 'SIGLA - DIVULGAÇÃO MAILING LIST', "fantasia" => 'SIGLA - DIVULGAÇÃO M', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 346, "codigo" => 'ET23438', "razao_social" => 'GUIGO 21 -  REPRESENTAÇÕES LTDA', "fantasia" => 'DIRCEU', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 356, "codigo" => 'FW23405', "razao_social" => 'DAMA REPRESENTAÇÕES E COMÉRCIO LTDA', "fantasia" => 'DAMA REPRESENTAÇÕES', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 357, "codigo" => 'FZ23418', "razao_social" => 'DAMA REPRESENTAÇÕES E COMÉRCIO LTDA', "fantasia" => 'DAMA REPRESENTAÇÕES', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 358, "codigo" => 'FY23421', "razao_social" => 'DAMA REPRESENTAÇÕES E COMÉRCIO LTDA', "fantasia" => 'DAMA REPRESENTAÇÕES', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 402, "codigo" => 'BK23479', "razao_social" => 'CASTRO REPRESENTAÇÕES LTDA', "fantasia" => 'CASTRO', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $obj = array("password" => Hash::make("somlivre"), "signus_id" => 404, "codigo" => 'DO23407', "razao_social" => 'NETTO VALENTIM REPR. S/S LTDA', "fantasia" => 'DILSON', 'fl_representante' => 1, "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($obj);
        $usuario = array("nome" => "Leonardo Medeiros (M2)", "email" => "leonardo@m2software.com.br", "created_at" => date("Y-m-d H:i:s"), "password" => Hash::make("windsurf"));
        DB::table('usuario')->insert($usuario);
        $usuario = array("nome" => "Rodrigo Moreno", "email" => "rodrigo.moreno@somlivre.com.br", "created_at" => date("Y-m-d H:i:s"));
        DB::table('usuario')->insert($usuario);
        DB::update(DB::raw('update usuario set nome = fantasia where nome is null'));
    }

    public function down() {
        if (Schema::hasColumn('usuario', 'codigo')) {
            Schema::table('usuario', function($table) {
                $table->dropColumn('codigo');
            });
        }
        if (Schema::hasColumn('usuario', 'signus_id')) {
            Schema::table('usuario', function($table) {
                $table->dropColumn('signus_id');
            });
        }
        if (Schema::hasColumn('usuario', 'razao_social')) {
            Schema::table('usuario', function($table) {
                $table->dropColumn('razao_social');
            });
        }
        if (Schema::hasColumn('usuario', 'fantasia')) {
            Schema::table('usuario', function($table) {
                $table->dropColumn('fantasia');
            });
        }
    }

}
