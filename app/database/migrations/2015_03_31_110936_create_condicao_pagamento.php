<?php

use Illuminate\Database\Migrations\Migration;

class CreateCondicaoPagamento extends Migration {
    public function up() {
        Schema::create('condicao_pagamento', function($table) {
            $table->integer('CondicaoPagamentoID');
            $table->string('Descricao')->nullable();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('condicao_pagamento');
    }
}