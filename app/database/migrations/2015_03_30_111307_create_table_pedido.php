<?php

use Illuminate\Database\Migrations\Migration;

class CreateTablePedido extends Migration {

    public function up() {
        Schema::create('pedido', function($table) {
            $table->increments('id_pedido');
            $table->integer('ClienteID')->unsigned();
            $table->integer('UnidadeNegocioID')->unsigned();
            $table->integer('CondicaoPagamentoID')->unsigned();
            $table->integer('numero')->unsigned();
            $table->integer('id_usuario_vendedor')->unsigned();
            $table->dateTime('dt_pedido')->nullable();
            $table->dateTime('dt_sincronizacao')->nullable();
            $table->dateTime('dt_envio')->nullable();
            $table->integer('id_signus')->unsigned()->nullable();
            $table->string('status_envio')->nullable();
            $table->string('msg_envio')->nullable();
            $table->timestamps();
        });

        Schema::create('item_pedido', function($table) {
            $table->increments('id_item_pedido');
            $table->integer('id_pedido')->unsigned();
            $table->integer('id_produto')->unsigned();
            $table->integer('qtd')->unsigned();
            $table->dateTime('dt_inclusao')->nullable();
            $table->decimal('perc_desconto', 5, 2)->unsigned()->nullable();
            $table->decimal('preco', 5, 2)->unsigned()->nullable();
            $table->timestamps();
        });                
    }

    public function down() {
        Schema::dropIfExists('pedido');
        Schema::dropIfExists('item_pedido');
    }
}