<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableProduto extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('produto', function($table) {
            $table->increments('id_produto');
            $table->integer('prod_cod')->nullable();
            $table->string('catalogo', 10);
            $table->string('titulo', 500);
            $table->string('artista', 500);
            $table->decimal('preco', 5, 2)->nullable();
            $table->string('imagem_destaque', 500)->nullable();
            $table->string('imagem_listagem', 500)->nullable();
            $table->boolean('icms_isento')->nullable();
            $table->boolean('coletanea')->nullable();
            $table->boolean('mais_vendido')->nullable();
            $table->boolean('infantil')->nullable();
            $table->boolean('lancamento')->nullable();
            $table->dateTime('dt_importacao')->nullable();
            $table->text('texto_cheio')->nullable();
            $table->text('texto_reduzido')->nullable();
            $table->integer('id_status_produto')->nullable();
            $table->dateTime('dt_liberacao')->nullable();
            $table->integer('id_usuario_liberacao')->nullable();
            $table->dateTime('dt_bloqueio')->nullable();
            $table->integer('id_usuario_bloqueio')->nullable();
            $table->integer('id_classe')->nullable();
            $table->string('codigo_barras', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('produto');
    }

}
