<?php

use Illuminate\Database\Migrations\Migration;

class CreateExtensaoTable extends Migration {

    public function up() {
        Schema::create('extensao', function($table) {
            $table->increments('id_extensao');
            $table->string('extensao');
            $table->boolean('fl_video');
            $table->string('mime', 500);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('extensao');
    }

}
