<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsuarioTable extends Migration {

    public function up() {
        Schema::create('usuario', function($table) {
            $table->increments('id_usuario');
            $table->string('nome', 500)->nullable();
            $table->string('password', 64)->nullable();
            $table->string('email', 500)->nullable();
            $table->boolean('fl_representante')->nullable();
            $table->dateTime('dt_ultimo_acesso')->nullable();
            $table->dateTime('dt_ultima_sincronizacao')->nullable();
            $table->text('observacao')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('usuario');
    }

}
