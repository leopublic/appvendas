<?php

use Illuminate\Database\Migrations\Migration;

class CreateMusicaTable extends Migration {

    public function up() {
        Schema::create('musica', function($table) {
            $table->increments('id_musica');
            $table->integer('id_produto')->unsigned()->nullable();
            $table->string('nome', 500)->nullable();
            $table->string('autor', 500)->nullable();
            $table->string('artista', 500)->nullable();
            $table->string('duracao', 20)->nullable();
            $table->integer('ordem')->nullable();
            $table->integer('id_extensao')->unsigned()->nullable();
            $table->string('nome_original', 1500)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('musica');
    }

}
