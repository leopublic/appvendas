<?php

use Illuminate\Database\Migrations\Migration;

class CreateTableStatusProduto extends Migration {
    public function up() {
        Schema::create('status_produto', function($table) {
            $table->increments('id_status_produto');
            $table->string('sigla', 2)->nullable();
            $table->string('descricao', 500)->nullable();
            $table->text('observacao')->nullable();
            $table->timestamps();
        });
        
        $obj = array("descricao" => "novo", "sigla" => "N", "observacao" => "Produto novo ainda não liberado");
        DB::table('status_produto')->insert($obj);
        $obj = array("descricao" => "liberado", "sigla" => "L", "observacao" => "Produto liberado para sincronização");
        DB::table('status_produto')->insert($obj);
    }

    public function down() {
        if (Schema::hasTable('status_produto')) {
            Schema::drop('status_produto');
        }
    }
}