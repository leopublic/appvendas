<?php
$app = new Illuminate\Foundation\Application;


/**
 * No item 'producao', informar no array o nome da m�quina e/ou o dom�nio. O nome da m�quina � obrigat�rio.
 */
$env = $app->detectEnvironment(array(
    'local' => array('*.dev'),
    'do' => array('ipanema'),
	'producao' => array('*.somlivre.com.br', 'rionf155'),
));


$app->bindInstallPaths(require __DIR__ . '/paths.php');


$framework = $app['path.base'] . '/vendor/laravel/framework/src';

require $framework . '/Illuminate/Foundation/start.php';

return $app;
